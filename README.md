# Academics RESTful Web Service

## Description

restng-academics has been converted into RESTng 2.0 requirements. RESTng 2.0 conversion task focued on fixing syntax or directory structure to meet RESTng 2.0. No functionality or logic change were made. student.php called "academic.banner.test.currentAcademicTerm.v2" which is under old academicTerm. Change needs to be made to call new academicTerm rewrite. 

After conversion was done, PHPUnit has been executed. It failed due to studentphp called old academicTerm.

## Database Tables / Operations

* szbuniq - SELECT
* sgrsatt - SELECT
* sgvsatt - SELECT
* sorlcur - SELECT
* stvcamp - SELECT
* stvclas - SELECT
* stvdegc - SELECT
* stvstyp - SELECT
* stvlevl - SELECT
* stvadmt - SELECT
* stvcoll - SELECT
* spriden - SELECT
* uachieve.job_queue_list - INSERT
* uachieve.stu_master - SELECT
* uachieve.stu_demo - SELECT
* uachieve.job_queue_run - SELECT
* uachieve.job_queue_out - SELECT
* sorlfos - SELECT
* stvmajr - SELECT
* dual - SELECT
* sfbetrm - SELECT
* sfbrgrp - SELECT
* sfbwctl - SELECT
* sfrwctl - SELECT

## Local Development Setup

1. pull down latest source code from this repository
2. install composer dependencies: `composer update`

## Testing

### Unit Testing

Unit test cases in this project is written using PHPUnit. 

`phpunit` should pass without any error message before and after making any change. Code coverage report will be
automatically generated after `phpunit` being ran and put into `test/coverage` folder.

## Documentation Links
* [Asset Information](https://miamioh.teamdynamix.com/TDNext/Apps/741/Assets/AssetDet?AssetID=427362)
* [DEV SwaggerUI](https://wsdev.apps.miamioh.edu/api/swagger-ui/#!/student/get_student_academics_v1)

### API Documentation

API documentation can be found on swagger page: <ws_url>/api/swagger-ui/#/student/get_student_academics_v1

### CAM

CAM entries for this Web Service can be found in CAM > WebServices > StudentAcademics