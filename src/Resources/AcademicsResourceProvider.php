<?php

namespace MiamiOH\RestngAcademics\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class AcademicsResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'student.academics.model',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'integer',
                ),
                'classCode' => array(
                    'type' => 'string',
                ),
                'className' => array(
                    'type' => 'string',
                ),
                'registrationFlag' => array(
                    'type' => 'string',
                ),
                'degrees' => array(
                    'type' => 'collection',
                    '$ref' => '#/definitions/student.academics.degree.collection',
                )
            ),
        ));

        $this->addDefinition(array(
            'name' => 'student.academics.collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/student.academics.model'
            )
        ));

        $this->addDefinition(array(
            'name' => 'student.academics.degree.model',
            'type' => 'object',
            'properties' => array(
                'id' => array(
                    'type' => 'string'
                ),
                'levelCode' => array(
                    'type' => 'string',
                ),
                'levelName' => array(
                    'type' => 'string',
                ),
                'degreeCode' => array(
                    'type' => 'string',
                ),
                'degreeName' => array(
                    'type' => 'string',
                ),
                'collegeCode' => array(
                    'type' => 'string',
                ),
                'collegeName' => array(
                    'type' => 'string',
                ),
                'campusCode' => array(
                    'type' => 'string',
                ),
                'campuseName' => array(
                    'type' => 'string',
                ),
                'typeCode' => array(
                    'type' => 'string',
                ),
                'typeName' => array(
                    'type' => 'string',
                ),
                'admitTypeCode' => array(
                    'type' => 'string',
                ),
                'admitTypeName' => array(
                    'type' => 'string',
                ),
                'programCode' => array(
                    'type' => 'string',
                ),
                'fieldOfStudy' => array(
                    'type' => 'collection'
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'student.academics.degree.collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/student.academics.degree.model'
            )
        ));

        $this->addDefinition(array(
            'name' => 'student.academics.degreeAuditJobID.collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/student.academics.degreeAuditJobID.model'
            )
        ));

        $this->addDefinition(array(
            'name' => 'student.academics.degreeAuditJobID.model',
            'type' => 'object',
            'properties' => array(
                'studentUid' => array(
                    'type' => 'string'
                ),
                'jobId' => array(
                    'type' => 'string',
                ),
                // 'auditStatus' => array(
                //     'type' => 'string',
                // ),
                'auditResource' => array(
                    'type' => 'string',
                )
            ),
        ));

        $this->addDefinition(array(
            'name' => 'student.academics.DAROutput.collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/student.academics.DAROutput.model'
            )
        ));

        $this->addDefinition(array(
            'name' => 'student.academics.DAROutput.model',
            'type' => 'object',
            'properties' => array(
                'studentUid' => array(
                    'type' => 'string'
                ),
                'jobId' => array(
                    'type' => 'string',
                ),
                'auditId' => array(
                    'type' => 'string',
                ),
                'auditStatus' => array(
                    'type' => 'string',
                ),
                'auditFormat' => array(
                    'type' => 'string',
                ),
                'auditOutput' => array(
                    'type' => 'string',
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'student.academics.gpa.term.model',
            'type' => 'object',
            'properties' => array(
                'studentUid' => array(
                    'type' => 'string'
                ),
                'termCode' => array(
                    'type' => 'string',
                ),
                'studentLevel' => array(
                    'type' => 'string',
                ),
                'gpaType' => array(
                    'type' => 'string',
                ),
                'hoursEarned' => array(
                    'type' => 'number',
                ),
                'gpaHours' => array(
                    'type' => 'number',
                ),
                'gpaQualityPoints' => array(
                    'type' => 'number',
                ),
                'gpa' => array(
                    'type' => 'number',
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'student.academics.gpa.term.collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/student.academics.gpa.term.model'
            )
        ));

        $this->addDefinition(array(
            'name' => 'student.academics.gpa.cumulative.model',
            'type' => 'object',
            'properties' => array(
                'studentUid' => array(
                    'type' => 'string'
                ),
                'studentLevel' => array(
                    'type' => 'string',
                ),
                'gpaType' => array(
                    'type' => 'string',
                ),
                'hoursEarned' => array(
                    'type' => 'number',
                ),
                'gpaHours' => array(
                    'type' => 'number',
                ),
                'gpaQualityPoints' => array(
                    'type' => 'number',
                ),
                'gpa' => array(
                    'type' => 'number',
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'student.academics.gpa.cumulative.collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/student.academics.gpa.cumulative.model'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'StudentAcademics',
            'class' => 'MiamiOH\RestngAcademics\Services\Student',
            'description' => 'Top level service for student academic information',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));

        $this->addService(array(
            'name' => 'StudentDegree',
            'class' => 'MiamiOH\RestngAcademics\Services\Degree',
            'description' => 'Academic Degrees for a student',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));

        $this->addService(array(
            'name' => 'StudentFieldOfStudy',
            'class' => 'MiamiOH\RestngAcademics\Services\FieldOfStudy',
            'description' => 'Majors/minors for a student',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));

        $this->addService(array(
            'name' => 'StudentSubfieldOfStudy',
            'class' => 'MiamiOH\RestngAcademics\Services\SubfieldOfStudy',
            'description' => 'Concentrations/Thematic sequences for a student',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));

        $this->addService(array(
            'name' => 'StudentAttributes',
            'class' => 'MiamiOH\RestngAcademics\Services\Attributes',
            'description' => 'Academic Attributes for a student',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));

        $this->addService(array(
            'name' => 'registrationTimeTicket',
            'class' => 'MiamiOH\RestngAcademics\Services\Student',
            'description' => 'Information about student registration time ticket',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'bannerUtil' => array('type' => 'service', 'name' => 'MU\BannerUtil'),
            )
        ));

        $this->addService(array(
            'name' => 'degreeAudit',
            'class' => 'MiamiOH\RestngAcademics\Services\DegreeAudit',
            'description' => 'Information about student degree audit (DAR)',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'bannerUtil' => array('type' => 'service', 'name' => 'MU\BannerUtil'),
                'validation' => array('name' => 'APIValidationFactory'),
            )
        ));

        $this->addService(array(
            'name' => 'StudentGpa',
            'class' => 'MiamiOH\RestngAcademics\Services\Gpa',
            'description' => 'GPA records for a student',
            'set' => array(
                'dataLoader' => array('type' => 'service', 'name' => 'Student.DataLoader'),
            )
        ));

        $this->addService(array(
            'name' => 'Student.DataLoader',
            'class' => 'MiamiOH\RestngAcademics\Data\DataLoaderOracle',
            'description' => 'Loads data from Oracle tables',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'student.academics.v1.student',
            'description' => 'Get student academic information',
            'pattern' => '/student/academics/v1/:uniqueId',
            'service' => 'StudentAcademics',
            'method' => 'getStudent',
            'returnType' => 'model',
            'params' => array(
                'uniqueId' => array('description' => 'uniqueId of the student'),
            ),
            'options' => array(
                'mode' => array('description' => 'record mode to return - defaults to LEARNER'),
                'termCode' => array('description' => 'termCode to use to collect data - defaults to "current"')
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'))
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'student.academics.v1.students',
            'description' => 'Get a collection of student academic information',
            'pattern' => '/student/academics/v1',
            'service' => 'StudentAcademics',
            'method' => 'getStudents',
            'isPartialable' => true,
            'returnType' => 'collection',
            'options' => array(
                'uniqueId' => array(
                    'type' => 'list',
                    'description' => 'uniqueId of the student'),
                'pidm' => array(
                    'type' => 'list',
                    'description' => 'pidm of the student'),
                'mode' => array('description' => 'record mode to return - defaults to LEARNER'),
                'termCode' => array('description' => 'termCode to use to collect data - defaults to "current"'),
                'display' => array('enum' => array('smart', 'normal'),
                    'default' => 'normal',
                    'description' => 'Specify the way data display - default to "normal"'),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of student academic information.',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/student.academics.collection',
                    )
                ),
                App::API_NOTFOUND => array(
                    'description' => 'There is no record found.',
                )
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'))
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'student.academics.v1.student.update',
            'description' => 'Update student academic information',
            'pattern' => '/student/academics/v1/:uniqueId',
            'service' => 'StudentAcademics',
            'method' => 'updateStudent',
            'returnType' => 'model',
            'params' => array(
                'uniqueId' => array('description' => 'uniqueId of the student'),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    'application' => 'WebServices',
                    'module' => 'StudentAcademics',
                    'key' => 'update')
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'student.academics.v1.degree',
            'description' => 'Get student degree information',
            'pattern' => '/student/academics/v1/:uniqueId/degree',
            'service' => 'StudentDegree',
            'method' => 'getDegree',
            'returnType' => 'collection',
            'params' => array(
                'uniqueId' => array('description' => 'uniqueId of the student'),
            ),
            'options' => array(
                'mode' => array('description' => 'record mode to return - defaults to LEARNER'),
                'termCode' => array('description' => 'termCode to use to collect data - defaults to "current"')
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'))
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'student.academics.v1.degrees',
            'description' => 'Get collection of students degree information',
            'pattern' => '/student/academics/v1/degree/',
            'service' => 'StudentDegree',
            'method' => 'getDegrees',
            'returnType' => 'collection',
            'options' => array(
                'uniqueId' => array(
                    'type' => 'list',
                    'description' => 'uniqueId of the student'
                ),
                'mode' => array('description' => 'record mode to return - defaults to LEARNER'),
                'termCode' => array('description' => 'termCode to use to collect data - defaults to "current"')
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of student degrees information.',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/student.academics.degree.collection',
                    )
                ),
                App::API_NOTFOUND => array(
                    'description' => 'There is no record found.',
                )
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'))
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'student.academics.v1.fieldOfStudy',
            'description' => 'Get student majors/minors information',
            'pattern' => '/student/academics/v1/:uniqueId/fieldOfStudy/:degId',
            'service' => 'StudentFieldOfStudy',
            'method' => 'getFieldOfStudy',
            'returnType' => 'collection',
            'params' => array(
                'uniqueId' => array('description' => 'uniqueId of the student'),
                'degId' => array('description' => 'id of degree'),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'))
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'student.academics.v1.subfieldOfStudy',
            'description' => 'Get student concentration/thematic sequence information',
            'pattern' => '/student/academics/v1/:uniqueId/subfieldOfStudy/:degId/:fosId',
            'service' => 'StudentSubfieldOfStudy',
            'method' => 'getSubfieldOfStudy',
            'returnType' => 'collection',
            'params' => array(
                'uniqueId' => array('description' => 'uniqueId of the student'),
                'degId' => array('description' => 'id of degree'),
                'fosId' => array('description' => 'id of field of study'),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'))
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'student.academics.v1.attributes',
            'description' => 'Get student attributes',
            'pattern' => '/student/academics/v1/:uniqueId/attributes',
            'service' => 'StudentAttributes',
            'method' => 'getAttributes',
            'returnType' => 'collection',
            'params' => array(
                'uniqueId' => array('description' => 'uniqueId of the student'),
            ),
            'options' => array(
                'termCode' => array('description' => 'termCode to use to collect data - defaults to "current"')
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'))
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'student.academics.v1.timetickets',
            'description' => 'Get student registration timeticket information',
            'pattern' => '/student/academics/v1/:muid/timeticket',
            'service' => 'registrationTimeTicket',
            'method' => 'checkTimeTicketByUniqueIdOrPidm',
            'returnType' => 'collection',
            'params' => array(
                'muid' => array('description' => "A Person's uniqueID or pidm",
                    'alternateKeys' => ['uniqueId', 'pidm']),
            ),
            'options' => array(
                'termCode' => array('description' => 'termCode to use to collect data')
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'))
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'student.academics.v1.degreeAudit.uid.create',
            'description' => 'Kick off the Degree Audit Report (DAR) generation process for a student',
            'pattern' => '/student/academics/v1/:muid/degreeAudit',
            'service' => 'degreeAudit',
            'method' => 'createDARForOne',
            'returnType' => 'model',
            'params' => array(
                'muid' => array('description' => "A Person's uniqueID or pidm",
                    'alternateKeys' => ['uniqueId', 'pidm'])
            ),
            'options' => array(
                'comkey' => array(
                    'description' => 'The term the student confirms admission to Miami University. OLD value allows course articulation based on the admit term code of the student (term_code_admit)',
                    'enum' => ['OLD']
                ),
                'evalsw' => array(
                    'description' => 'Determines whether to only evaluate test scores into Miami-equivalent courses (E), or whether to also perform other operations in addition evaluation. Sample value is E, to evaluate AP test scores in Miami-course credit.'
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Job ID for the submitted DAR generation.',
                    'returns' => array(
                        'type' => 'object',
                        '$ref' => '#/definitions/student.academics.degreeAuditJobID.model',
                    )
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'The user could not be authenticated or is not authorized to access the resource.'
                ),
                App::API_BADREQUEST => array(
                    'description' => 'The request was incorrectly formed.'
                ),
                App::API_FAILED => array(
                    'description' => 'Internal server error',
                )
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'type' => 'authMan',
                        'application' => 'WebServices',
                        'module' => 'StudentDegreeAudit',
                        'key' => array('all', 'create')
                    ),
                    array(
                        'type' => 'self',
                        'param' => 'muid'
                    ),
                )
            )
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'student.academics.v1.degreeAudit.create',
            'description' => 'RESTng 2.0 - Kick off the DAR generation process for one or multiple students',
            'pattern' => '/student/academics/v1/degreeAudit',
            'service' => 'degreeAudit',
            'method' => 'createDARForMultiples',
            'returnType' => 'collection',
            'params' => array(),
            'options' => array(
                'uniqueId' => array(
                    'description' => 'Comma-separated list of Unique ID(s) of the students whose DARs are to be generated',
                    'type' => 'list'
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Job ID collection for the submitted DAR generation.',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/student.academics.degreeAuditJobID.collection',
                    )
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'The user could not be authenticated or is not authorized to access the resource.'
                ),
                App::API_BADREQUEST => array(
                    'description' => 'The request was incorrectly formed.'
                ),
                App::API_FAILED => array(
                    'description' => 'Internal server error',
                )

            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'type' => 'authMan',
                        'application' => 'WebServices',
                        'module' => 'StudentDegreeAudit',
                        'key' => array('all', 'create')
                    )
                )
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'student.academics.v1.degreeAudit.jobid.read',
            'description' => 'RESTng 2.0 - Get DAR(s) from a job ID.',
            'pattern' => '/student/academics/v1/degreeAudit/:jobId',
            'service' => 'degreeAudit',
            'method' => 'getDARFromJobID',
            'returnType' => 'collection',
            'params' => array(
                'jobId' => array('description' => 'DAR job ID.')
            ),
            'options' => array(
                'auditFormat' => array(
                    'description' => 'Format of the DAR output: Single/Double coloumn.',
                    'enum' => ['single', 'double']
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'DAR is generated and returned successfully.',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/student.academics.DAROutput.collection',
                    )
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'The user could not be authenticated or is not authorized to access the resource.'
                ),
                App::API_BADREQUEST => array(
                    'description' => 'The request was incorrectly formed.'
                ),
                App::API_FAILED => array(
                    'description' => 'Internal server error',
                )

            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'type' => 'authMan',
                        'application' => 'WebServices',
                        'module' => 'StudentDegreeAudit',
                        'key' => array('all', 'read')
                    )
                )
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'student.academics.v1.gpa',
            'description' => 'Get student GPA information',
            'pattern' => '/student/academics/v1/:uniqueId/gpa',
            'service' => 'StudentGpa',
            'method' => 'getGpa',
            'returnType' => 'collection',
            'params' => array(
                'uniqueId' => array('description' => 'uniqueId of the student'),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token')
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'GPA data request was correctly processed and results are included (may be empty).',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/student.academics.gpa.term.collection',
                    )
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'The user could not be authenticated or is not authorized to access the resource.'
                ),
                App::API_BADREQUEST => array(
                    'description' => 'The request was incorrectly formed.'
                ),
                App::API_FAILED => array(
                    'description' => 'Internal server error',
                )
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'student.academics.v1.gpa.cumulative',
            'description' => 'Get cumulative student GPA information',
            'pattern' => '/student/academics/v1/:uniqueId/gpa/cumulative',
            'service' => 'StudentGpa',
            'method' => 'getGpaCumulative',
            'returnType' => 'collection',
            'params' => array(
                'uniqueId' => array('description' => 'uniqueId of the student'),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token')
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'GPA data request was correctly processed and results are included (may be empty).',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/student.academics.gpa.cumulative.collection',
                    )
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'The user could not be authenticated or is not authorized to access the resource.'
                ),
                App::API_BADREQUEST => array(
                    'description' => 'The request was incorrectly formed.'
                ),
                App::API_FAILED => array(
                    'description' => 'Internal server error',
                )
            ),
        ));
    }

    public function registerOrmConnections(): void
    {

    }
}
