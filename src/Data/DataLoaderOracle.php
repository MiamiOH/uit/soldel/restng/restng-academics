<?php

namespace MiamiOH\RestngAcademics\Data;



use MiamiOH\RESTng\Exception\InvalidArgumentException;
use MiamiOH\RESTng\Legacy\DB\DBH\OCI8;

class DataLoaderOracle implements DataLoader
{
    private $datasourceName = 'MUWS_GEN_PROD';

    /** @var OCI8 */
    private $handle;

    public function setDatabase($database)
    {
        $this->database = $database;
        $this->handle = $this->database->getHandle($this->datasourceName);
        $this->handle->mu_trigger_error = false;
    }

    /**
     * @inheritdoc
     */
    public function getPidmFromUniqueId(string $uniqueId): string
    {
        $pidm = $this->handle->queryfirstcolumn('
            SELECT szbuniq_pidm FROM szbuniq WHERE szbuniq_unique_id = ?',
            strtoupper($uniqueId)
        );

        if ($pidm === DB_EMPTY_SET) {
            throw new InvalidArgumentException(sprintf('UniqueID %s could not be found', $uniqueId));
        }

        return $pidm;
    }

    public function getGpaRecords(string $pidm): array
    {
        return $this->handle->queryall_array('
            select lower(szbuniq_unique_id) as uniqueid, 
                   shrtgpa_term_code as term_code, 
                   shrtgpa_levl_code as student_level, 
                   shrtgpa_gpa_type_ind as type_indicator, 
                   shrtgpa_hours_earned as hours_earned, 
                   shrtgpa_gpa_hours as gpa_hours,
                   shrtgpa_quality_points as quality_points, 
                   shrtgpa_gpa as gpa
                from shrtgpa inner join szbuniq on szbuniq_pidm = shrtgpa_pidm
                where shrtgpa_pidm = ?
        ', $pidm);
    }

    public function getGpaCumulativeRecords(string $pidm): array
    {
        return $this->handle->queryall_array('
            select lower(szbuniq_unique_id) as uniqueid, 
                   shrlgpa_levl_code as student_level, 
                   shrlgpa_gpa_type_ind as type_indicator, 
                   shrlgpa_hours_earned as hours_earned, 
                   shrlgpa_gpa_hours as gpa_hours,
                   shrlgpa_quality_points as quality_points, 
                   shrlgpa_gpa as gpa
                from shrlgpa inner join szbuniq on szbuniq_pidm = shrlgpa_pidm
                where shrlgpa_pidm = ?
        ', $pidm);
    }
}
