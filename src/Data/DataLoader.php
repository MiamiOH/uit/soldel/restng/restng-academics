<?php

namespace MiamiOH\RestngAcademics\Data;

use MiamiOH\RESTng\Exception\InvalidArgumentException;

interface DataLoader
{
    /**
     * @throws InvalidArgumentException
     */
    public function getPidmFromUniqueId(string $uniqueId): string;

    public function getGpaRecords(string $pidm): array;

    public function getGpaCumulativeRecords(string $pidm): array;
}
