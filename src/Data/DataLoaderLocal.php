<?php

namespace MiamiOH\RestngAcademics\Data;

use MiamiOH\RESTng\Exception\InvalidArgumentException;

class DataLoaderLocal implements DataLoader
{

    /**
     * @inheritdoc
     */
    public function getPidmFromUniqueId(string $uniqueId): string
    {
        if ($uniqueId === 'doej') {
            return '123456';
        }

        throw new InvalidArgumentException(sprintf('UniqueID %s could not be found', $uniqueId));
    }

    public function getGpaRecords(string $pidm): array
    {
        if ($pidm === '123456') {
            return [
                [
                    'uniqueid' => 'doej',
                    'term_code' => '202210',
                    'student_level' => 'UG',
                    'type_indicator' => 'I',
                    'hours_earned' => 16,
                    'gpa_hours' => 16,
                    'quality_points' => 41,
                    'gpa' => 2.56,
                ]
            ];
        }

        return [];
    }

    public function getGpaCumulativeRecords(string $pidm): array
    {
        if ($pidm === '123456') {
            return [
                [
                    'uniqueid' => 'doej',
                    'student_level' => 'UG',
                    'type_indicator' => 'I',
                    'hours_earned' => 16,
                    'gpa_hours' => 16,
                    'quality_points' => 41,
                    'gpa' => 2.56,
                ]
            ];
        }

        return [];
    }
}
