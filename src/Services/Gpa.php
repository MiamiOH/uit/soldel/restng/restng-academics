<?php

namespace MiamiOH\RestngAcademics\Services;

use MiamiOH\RESTng\Exception\InvalidArgumentException;
use MiamiOH\RestngAcademics\Data\DataLoader;

class Gpa extends \MiamiOH\RESTng\Service
{
    /**
     * @var DataLoader
     */
    private $dataLoader;

    public function setDataLoader(DataLoader $dataLoader): void
    {
        $this->dataLoader = $dataLoader;
    }

    public function getGpa()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $uniqueId = $request->getResourceParam('uniqueId');

        $authUser = $this->getApiUser()->getUsername();

        if (strtolower($authUser) != strtolower($uniqueId)) {
            if (!$this->getApiUser()->isAuthorized('WebServices', 'StudentGpa', 'view') &&
                !$this->getApiUser()->isAuthorized('WebServices', 'StudentGpa', 'All')) {
                $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
                return $response;
            }
        }

        try {
            $pidm = $this->dataLoader->getPidmFromUniqueId($uniqueId);
        } catch (InvalidArgumentException $e) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        $payload = $this->getGpaRecords($pidm);

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    private function getGpaRecords(string $pidm): array
    {
        $records = $this->dataLoader->getGpaRecords($pidm);

        return array_map(function (array $record) {
            return [
                'uniqueId' => $record['uniqueid'],
                'termCode' => $record['term_code'],
                'studentLevel' => $record['student_level'],
                'gpaType' => $record['type_indicator'],
                'hoursEarned' => $record['hours_earned'],
                'gpaHours' => $record['gpa_hours'],
                'gpaQualityPoints' => $record['quality_points'],
                'gpa' => $record['gpa'],
            ];
        }, $records);
    }

    public function getGpaCumulative()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $uniqueId = $request->getResourceParam('uniqueId');

        $authUser = $this->getApiUser()->getUsername();

        if (strtolower($authUser) != strtolower($uniqueId)) {
            if (!$this->getApiUser()->isAuthorized('WebServices', 'StudentGpa', 'view') &&
                !$this->getApiUser()->isAuthorized('WebServices', 'StudentGpa', 'All')) {
                $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
                return $response;
            }
        }

        try {
            $pidm = $this->dataLoader->getPidmFromUniqueId($uniqueId);
        } catch (InvalidArgumentException $e) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        $payload = $this->getGpaCumulativeRecords($pidm);

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    private function getGpaCumulativeRecords(string $pidm): array
    {
        $records = $this->dataLoader->getGpaCumulativeRecords($pidm);

        return array_map(function (array $record) {
            return [
                'uniqueId' => $record['uniqueid'],
                'studentLevel' => $record['student_level'],
                'gpaType' => $record['type_indicator'],
                'hoursEarned' => $record['hours_earned'],
                'gpaHours' => $record['gpa_hours'],
                'gpaQualityPoints' => $record['quality_points'],
                'gpa' => $record['gpa'],
            ];
        }, $records);
    }
}
