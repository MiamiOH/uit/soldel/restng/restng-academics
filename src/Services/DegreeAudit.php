<?php


namespace MiamiOH\RestngAcademics\Services;

use Illuminate\Support\Facades\Log;
use MiamiOH\RESTng\Service\Extension\BannerIdNotFound;
use MiamiOH\RESTng\Service\Extension\BannerIdTooManyMatches;


class DegreeAudit extends \MiamiOH\RESTng\Service
{

    private $dbDataSourceName = 'MUWS_GEN_PROD';
    private $dbh;
    private $validator;

    const INSTIDQ = '73';
    const INSTID = '007104';

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dbDataSourceName);
    }

    public function setBannerUtil($bannerUtil)
    {
        /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil $bannerUtil */
        $this->bannerUtil = $bannerUtil;
    }

    public function setValidation($validation)
    {
        $this->validator = $validation;
    }


    public function createDARForOne()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();

        $comkeyStudentConfirmedSemester = '';
        $evaluationSwitch = '';
        if (isset($options['comkey'])) {
            $comkeyStudentConfirmedSemester = $options['comkey'];
        }
        if (isset($options['evalsw'])) {
            $evaluationSwitch = $options['evalsw'];
        }


        //get the :muid param
        $idType = $request->getResourceParamKey('muid');
        $id = $request->getResourceParam('muid');

        //validate the :muid param
        $this->validateMUID($idType, $id);

        //get user ids
        $person = $this->bannerUtil->getId($idType, $id);
        $uid = $person->getUniqueId();
        $pidm = $person->getPidm();
        $bannerId = $person->getBannerId();

        //generate the Job ID
        $jobID = 'WS' . date('YmdHis');

        $this->createAndKickOffJob($jobID, $bannerId, $pidm, $comkeyStudentConfirmedSemester, $evaluationSwitch);

        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);
        $response->setPayload($this->buildPOSTResponse($uid, $jobID));

        return $response;

    }

    public function createDARForMultiples()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();

        if (isset($options['uniqueId'])) {
            $uids = $options['uniqueId'];
            $uidValidator = $this->validator->newValidator()->regex('/^\w{1,8}$/');
            $this->validateInput($uids, $uidValidator, "One or more unique IDs are invalid.");

            //generate the Job ID
            $jobID = 'WS' . date('YmdHis');

            try {
                $this->insertJobQueueListRecord($jobID);

                foreach ($uids as $uid) {
                    $person = $this->bannerUtil->getId('uniqueId', $uid);
                    $uid = $person->getUniqueId();
                    $pidm = $person->getPidm();
                    $bannerId = $person->getBannerId();

                    $this->insertJobQueueDetailRecord($jobID, $bannerId, $pidm);
                    $this->checkStudentRecord($bannerId, $pidm, $this->getFullNameFromPidm($pidm));
                }
                $this->kickOffAudit($jobID);
                $this->dbh->commit();

                //move the stu_demo check/update to after stu_master check/update has been committed to the db
                //that way if a student didn't exist in stu_master at all, it will no longer error out trying to get the int_seq_no from the record
                foreach($uids as $uid)
                {
                    $this->checkStudentDemoRecord($pidm, $this->getFullNameFromPidm($pidm));
                }
                $this->dbh->commit();

            } catch(\MiamiOH\RESTng\Service\Extension\BannerIdTooManyMatches $e){
                $this->dbh->rollback();
                throw new \Exception("Creating degree audit job failed: Too many matches for '$uid' | " . $e->getMessage());
            }
            catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e){
                $this->dbh->rollback();
                throw new \Exception("Creating degree audit job failed: Unique id '$uid' not found | " . $e->getMessage());
            }
            catch (\Exception $e) {
                $this->dbh->rollback();
                throw new \Exception("Creating degree audit job failed: Error updating database for user '$uid' degree audit | " . $e->getMessage());
            }
        } else {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Unique IDs are required.');
        }

        //set the payload
        $payload = [];
        foreach ($uids as $uid) {
            $payload[] = $this->buildPOSTResponse($uid, $jobID);
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);
        $response->setPayload($payload);

        return $response;

    }

    private function createAndKickOffJob($jobID, $bannerId, $pidm, $comkeyStudentConfirmedSemester = null, $evaluationSwitch = null)
    {
        try {
            $this->insertJobQueueListRecord($jobID, $comkeyStudentConfirmedSemester);
            $this->insertJobQueueDetailRecord($jobID, $bannerId, $pidm, $comkeyStudentConfirmedSemester, $evaluationSwitch);
            $this->checkStudentRecord($bannerId, $pidm, $this->getFullNameFromPidm($pidm));
            $this->kickOffAudit($jobID);

            $this->dbh->commit();

            //move the stu_demo check/update to after stu_master check/update has been committed to the db
            //that way if a student didn't exist in stu_master at all, it will no longer error out trying to get the int_seq_no from the record
            $this->checkStudentDemoRecord($pidm, $this->getFullNameFromPidm($pidm));
            $this->dbh->commit();

        } catch (\Exception $e) {
            $this->dbh->rollback();
            throw new \Exception("Creating degree auditing job failed." . $e->getMessage());
        }

    }

    private function getFullNameFromPidm($pidm)
    {
        try {
            $studentRecord = $this->dbh->queryall_array(
                "SELECT spriden_last_name || ', ' || spriden_first_name fullname  
               FROM spriden 
              WHERE spriden_pidm = ?
                AND spriden_change_ind IS NULL",
                $pidm);

            if (count($studentRecord) > 0) {
                return $studentRecord[0]['fullname'];
            } else {
                return 'UNKNOWN';
            }
        } catch (\Exception $e) {
            return 'UNKNOWN';
        }

    }

    private function buildPOSTResponse($uid, $jobID)
    {
        $response = [];
        $response['studentUid'] = $uid;
        $response['jobId'] = $jobID;
        $response['auditResource'] = "/$uid/$jobID";

        return $response;

    }


    private function validateMUID($idType, $id)
    {
        switch ($idType) {
            case 'uniqueId':
                $uidValidator = $this->validator->newValidator()->regex('/^\w{1,8}$/');
                $this->validateInput($id, $uidValidator, 'Invalid unique ID.');
                break;
            case 'pidm':
                $pidmValidator = $this->validator->newValidator()->regex('/^\d{1,8}$/');
                $this->validateInput($id, $pidmValidator, 'Invalid pidm.');
                break;
        }
    }


    private function validateInput($input, $validator, $msg)
    {
        if (is_array($input)) {
            foreach ($input as $value) {
                try {
                    $validator->assert($value);
                } catch (\Exception $exception) {
                    throw new \MiamiOH\RESTng\Exception\BadRequest($msg);
                }
            }
        } else {
            try {
                $validator->assert($input);
            } catch (\Exception $exception) {
                throw new \MiamiOH\RESTng\Exception\BadRequest($msg);
            }
        }

    }


    private function insertJobQueueListRecord($jobID, $comkeyStudentConfirmedSemester = null)
    {
        if (!is_null($comkeyStudentConfirmedSemester) && !empty($comkeyStudentConfirmedSemester)) {
            $sql = "INSERT INTO uachieve.job_queue_list (instidq, instid, jobid, userid, status, priority, servername,
                                                last_mod_user, last_mod_date, startdate, startTime, log_level, report_type, comkey)
                VALUES (?, ? , ?, ?, ?, ?, ?, ?, SYSDATE, ?, ?, ?, ?, ?)";

            $this->dbh->perform($sql, self::INSTIDQ, self::INSTID, $jobID, 'WS_USER', 'H', 75, 'CLIENT', 'WS_USER', ' ', ' ', 'O', 'TXT', $comkeyStudentConfirmedSemester);
            return true;
        }

        $sql = "INSERT INTO uachieve.job_queue_list (instidq, instid, jobid, userid, status, priority, servername,
                                                last_mod_user, last_mod_date, startdate, startTime, log_level, report_type)
                VALUES (?, ? , ?, ?, ?, ?, ?, ?, SYSDATE, ?, ?, ?, ?)";

        $this->dbh->perform($sql, self::INSTIDQ, self::INSTID, $jobID, 'WS_USER', 'H', 75, 'CLIENT', 'WS_USER', ' ', ' ', 'O', 'TXT');

        return true;
    }



    private function insertJobQueueDetailRecord($jobID, $bannerId, $pidm, $comkeyStudentConfirmedSemester = null, $evaluationSwitch = null)
    {
        if (!is_null($comkeyStudentConfirmedSemester) && !is_null($evaluationSwitch)) {
            if (!empty($comkeyStudentConfirmedSemester) && !empty($evaluationSwitch)) {
                $sql = "INSERT INTO uachieve.job_queue_detail (instidq, instid, jobid, userid, user_seq_no, stuno, comkey,
                                                  evalsw, test, soprid)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                $this->dbh->perform($sql, self::INSTIDQ, self::INSTID, $jobID, 'WS_USER', 1, $bannerId, $comkeyStudentConfirmedSemester, $evaluationSwitch, ' ', $pidm);
                return true;
            }
        }
        $sql = "INSERT INTO uachieve.job_queue_detail (instidq, instid, jobid, userid, user_seq_no, stuno, comkey,
                                                  evalsw, test, soprid)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        $this->dbh->perform($sql, self::INSTIDQ, self::INSTID, $jobID, 'WS_USER', 1, $bannerId, 'IA   ', 'S', ' ',
            $pidm);

    }


    private function checkStudentRecord($bannerId, $pidm, $stuFullName)
    {
        $queryString = "SELECT int_seq_no 
               FROM uachieve.stu_master 
              WHERE pidm = ?";

        $studentMasterRec = $this->dbh->queryall_array($queryString, $pidm);

        if (count($studentMasterRec) <= 0) {
            //make sure the student isn't already in the table without a pidm
            $queryString = "SELECT int_seq_no 
               FROM uachieve.stu_master 
              WHERE stuno = ?";

            $studentMasterRec = $this->dbh->queryall_array($queryString, $bannerId);

            if (count($studentMasterRec) <= 0) {
                //if no record found, insert one
                $sql = "INSERT INTO uachieve.stu_master(instidq, instid, stuno, pidm) VALUES (?, ?, ?, ?)";
                $this->dbh->perform($sql, self::INSTIDQ, self::INSTID, $bannerId, $pidm);

                $studentMasterRec = $this->dbh->queryall_array($queryString, $pidm);
            }
            else {
                //if record found, update it to add the pidm
                //and don't query again since we already have the record in $studentMasterRec
                $sql = "UPDATE uachieve.stu_master SET pidm = ? WHERE instidq = ? AND instid = ? AND stuno = ?";
                $this->dbh->perform($sql, $pidm, self::INSTIDQ, self::INSTID, $bannerId);
            }
        }
    }

    private function checkStudentDemoRecord($pidm, $stuFullName)
    {
        $queryString = "SELECT int_seq_no 
               FROM uachieve.stu_master 
              WHERE pidm = ?";

        $studentMasterRec = $this->dbh->queryall_array($queryString, $pidm);
        $stuMasterNo = $studentMasterRec[0]['int_seq_no'];

        $studentDemoRec = $this->dbh->queryall_array(
            "SELECT stuname  
               FROM uachieve.stu_demo 
              WHERE stu_mast_no = ?",
            $stuMasterNo);


        if (count($studentDemoRec) <= 0) {
            //if no record found, insert one
            $sql = "INSERT INTO uachieve.stu_demo(stu_mast_no, source_id, stuidq, stuname) VALUES (?, ?, ?, ?)";
            $this->dbh->perform($sql, $stuMasterNo, self::INSTID, self::INSTIDQ, $stuFullName);
        }
    }

    private function kickOffAudit($jobID)
    {
        $sql = "UPDATE uachieve.job_queue_list SET status = 'N'  WHERE jobid = ?";
        $this->dbh->perform($sql, $jobID);

    }


    public function getDARFromJobID()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();


        //get the :jobid param
        $jobID = $request->getResourceParam('jobId');

        $this->validateJobID($jobID);

        //check options
        if (isset($options['auditFormat'])) {
            $auditFormat = $options['auditFormat'];

            if (($auditFormat !== 'single') && ($auditFormat !== 'double')) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Invalid audit format.');
            }
        } else {
            //default value
            $auditFormat = 'double';
        }
        if (!$this->checkJobIDExist($jobID)) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        $auditIDs = $this->getAuditIDs($jobID, $auditFormat);

        if (count($auditIDs) <= 0) {
            $payload = $this->buildGETResponseNotCompleted($jobID);
        } else {
            $payload = $this->getDarOutput($auditIDs, $jobID, $auditFormat);
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;

    }


    private function buildGETResponseNotCompleted($jobID)
    {
        $degreeAuditResults = [];

        $darRecord = [];

        $darRecord['studentUid'] = '';
        $darRecord['jobId'] = $jobID;
        $darRecord['auditId'] = '';
        $darRecord['auditDegree'] = '';
        $darRecord['auditStatus'] = 'Not Completed';
        $darRecord['auditFormat'] = '';
        $darRecord['auditOutput'] = '';

        $degreeAuditResults[] = $darRecord;

        return $degreeAuditResults;
    }


    private function checkJobIDExist($jobID)
    {
        $sqlCheckJobId = "SELECT jobid FROM uachieve.job_queue_list WHERE  jobid = ?";

        $intSeqnoResult = $this->dbh->queryall_array($sqlCheckJobId, $jobID);

        if (count($intSeqnoResult) == 0) {
            return false;
        }
        return true;
    }


    private function getAuditIDs($jobID, $auditFormat)
    {

        if ($auditFormat == 'single' || $auditFormat == 'double') {
            if ($auditFormat == 'single') {
                $auditFormatCode = 'W';
            } elseif ($auditFormat == 'double') {
                $auditFormatCode = '2';
            }
        }

        $sql = "SELECT INT_SEQ_NO, SZBUNIQ_UNIQUE_ID, DPTITLE1 as degree FROM uachieve.job_queue_run, szbuniq 
                WHERE RTRIM(stuno) = szbuniq_banner_id AND jobid = ? AND NCOL = ?";

        $intSeqnoList = $this->dbh->queryall_array($sql, $jobID, $auditFormatCode);

        return $intSeqnoList;

    }


    private function getDarOutput($auditIDs, $jobID, $auditFormat)
    {

        $degreeAuditresults = [];

        if ($auditFormat == 'single') {
            $sqlForDarOut = "SELECT darout FROM uachieve.job_queue_out WHERE jobq_seq_no = ? AND (optline!='W' OR optline is null)";
        } else {
            $sqlForDarOut = "SELECT darout FROM uachieve.job_queue_out WHERE jobq_seq_no = ?";
        }

        foreach ($auditIDs as $auditRecord) {

            $degreeAuditList = [];
            $auditId = $auditRecord['int_seq_no'];

            $queueOutList = $this->dbh->queryall_array($sqlForDarOut, $auditId);

            $darOutString = '';
            foreach ($queueOutList as $queueOutRecord) {
                $darOutString = $darOutString . $queueOutRecord['darout'] . "\n";
            }
            $darOutString = trim($darOutString);
            $darRecord['studentUid'] = $auditRecord['szbuniq_unique_id'];
            $darRecord['jobId'] = $jobID;
            $darRecord['auditId'] = $auditId;
            $darRecord['auditDegree'] = trim($auditRecord['degree']);
            $darRecord['auditStatus'] = 'Completed';
            $darRecord['auditFormat'] = ucwords($auditFormat) . ' Column';
            $darRecord['auditOutput'] = $darOutString;

            $degreeAuditresults[] = $darRecord;
        }

        return $degreeAuditresults;
    }


    private function validateJobID($jobId)
    {
        //check if jobId is valid
        $jobIdValidator = $this->validator->newValidator()->regex('/^[[:alnum:]]+$/');
        $this->validateInput($jobId, $jobIdValidator, 'Invalid Job ID.');

    }
}
