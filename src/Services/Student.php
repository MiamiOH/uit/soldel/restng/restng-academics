<?php

namespace MiamiOH\RestngAcademics\Services;

use DateTime;

class Student extends \MiamiOH\RESTng\Service
{

    private $datasource_name = 'MUWS_GEN_PROD';
    private $dbh = '';
    private $validator;
    private $bannerUtil;
   // private $response;


    public function setBannerUtil($bannerUtil)
    {
        /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil $bannerUtil */
        $this->bannerUtil = $bannerUtil;
    }

    public function getStudent()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $uniqueId = $request->getResourceParam('uniqueId');

        $options = $request->getOptions();

        if (!isset($options['mode'])) {
            $options['mode'] = 'LEARNER';
        }
        if (isset($options['termCode']) &&
            $options['termCode'] != 'current' &&
            $options['termCode'] != 'max' &&
            !preg_match('/^\d\d\d\d\d\d$/', $options['termCode'])
        ) {
            $response->setPayload(array('message' => 'Invalid termCode option'));
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);

            return $response;
        }

        $authUser = $this->getApiUser()->getUsername();

        if (strtolower($authUser) != strtolower($uniqueId)) {
            if (!$this->getApiUser()->isAuthorized('WebServices', 'StudentAcademics', 'view') &&
                !$this->getApiUser()->isAuthorized('WebServices', 'StudentAcademics', 'All')
            ) {
                $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);

                return $response;
            }
        }

        $this->dbh = $this->database->getHandle($this->datasource_name);
        $this->dbh->mu_trigger_error = false;

        $pidm = $this->dbh->queryfirstcolumn("SELECT szbuniq_pidm FROM szbuniq WHERE szbuniq_unique_id = ?",
            strtoupper($uniqueId));
        if ($pidm === DB_EMPTY_SET) {
            $response->setPayload(array('message' => 'User not found'));
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);

            return $response;
        }

        $currentTerm = $this->dbh->queryfirstcolumn("SELECT fz_get_term() FROM dual");
        if ($currentTerm === DB_EMPTY_SET) {
            $response->setPayload(array('message' => 'Unexpected error getting current term'));
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);

            return $response;
        }

        $returnArray = array();

        $dResponse = $this->callResource('student.academics.v1.degree',
            array(
                'params' => array('uniqueId' => $uniqueId),
                'options' => $options
            ));
        if ($dResponse->getStatus() !== \MiamiOH\RESTng\App::API_OK) {
            $response->setPayload(array('message' => 'Unexpected error when retrieving degrees'));
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);

            return $response;
        }
        $dPayload = $dResponse->getPayload();

        $aResponse = $this->callResource(
            'student.academics.v1.attributes',
            array(
                'params' => array('uniqueId' => $uniqueId),
                'options' => array('termCode' => (isset($options['termCode']) ? $options['termCode'] : 'current'))
            )
        );

        if ($aResponse->getStatus() !== \MiamiOH\RESTng\App::API_OK) {
            $response->setPayload(array('message' => 'Unexpected error when retrieving attributes'));
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);

            return $response;
        }
        $aPayload = $aResponse->getPayload();

        // Primary campus and level should come from the first circulum record
        $returnArray['campusCode'] = ($dPayload[0]['campusCode'] ? $dPayload[0]['campusCode'] : '');
        $returnArray['campusName'] = ($dPayload[0]['campusName'] ? $dPayload[0]['campusName'] : '');
        $returnArray['levelCode'] = ($dPayload[0]['levelCode'] ? $dPayload[0]['levelCode'] : '');
        $returnArray['levelName'] = ($dPayload[0]['levelName'] ? $dPayload[0]['levelName'] : '');

        $class = $this->dbh->queryfirstrow_assoc(
            "SELECT fz_class_calc(?, ?, ?, '')
                                AS class_code,
              stvclas_desc      AS class_name
         FROM dual
    LEFT JOIN stvclas
           ON stvclas_code = fz_class_calc(?, ?, ?, '')",
            $pidm,
            $returnArray['levelCode'],
            (!isset($options['termCode']) || $options['termCode'] == 'current' || $options['termCode'] == 'max' ? $currentTerm : $options['termCode']),
            $pidm,
            $returnArray['levelCode'],
            (!isset($options['termCode']) || $options['termCode'] == 'current' || $options['termCode'] == 'max' ? $currentTerm : $options['termCode'])
        );

        $returnArray['classCode'] = trim($class['class_code']);
        $returnArray['className'] = trim($class['class_name']);

        // Check for conditionalAdmitProgramCode
        $returnArray['conditionalAdmitProgramCode'] = '';
        $returnArray['conditionalAdmitProgramName'] = '';
        foreach ($aPayload as $attribute) {
            if ($attribute['code'] == 'ACE') {
                $returnArray['conditionalAdmitProgramCode'] = 'ACE';
                $returnArray['conditionalAdmitProgramName'] = 'American Culture and English Program';
            }
            if ($attribute['code'] == 'RELC') {
                $returnArray['conditionalAdmitProgramCode'] = 'ELC4';
                $returnArray['conditionalAdmitProgramName'] = 'English Learning Center Program - Level 4';
            }
        }
        foreach ($dPayload as $degree) {
            if ($degree['degreeCode'] == '999' &&
                $degree['levelCode'] == 'CR' &&
                $degree['admitTypeCode'] == 'RI' &&
                $degree['collegeCode'] == 'NC'
            ) {
                $returnArray['conditionalAdmitProgramCode'] = 'ELC1-3';
                $returnArray['conditionalAdmitProgramName'] = 'English Learning Center Program - Level 1-3';
            }
        }

        $returnArray['degrees'] = $dPayload;
        $returnArray['attributes'] = $aPayload;

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($returnArray);

        return $response;
    }

    private $fields = array(
        'pidm',
        'uniqueId',
        'classCode',
        'className',
        'registrationFlag',
        'degrees'
    );

    public function getStudents()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $authUser = $this->getApiUser()->getUsername();

        if (!$this->getApiUser()->isAuthorized('WebServices', 'StudentAcademics', 'view') &&
            !$this->getApiUser()->isAuthorized('WebServices', 'StudentAcademics', 'All')
        ) {
            $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);

            return $response;
        }

        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        if (!isset($options['mode']) || !$options['mode']) {
            $options['mode'] = 'LEARNER';
        }

        if (!isset($options['termCode']) || !$options['termCode']) {
            $options['termCode'] = 'current';
        } elseif (isset($options['termCode']) &&
            $options['termCode'] != 'current' &&
            $options['termCode'] != 'max' &&
            !preg_match('/^\d{6}$/', $options['termCode'])
        ) {
            throw new \MiamiOH\RESTng\Exception\BadRequest("Invalid termCode option");
        }

        if (!$this->dbh) {
            $this->dbh = $this->database->getHandle($this->datasource_name);
            $this->dbh->mu_trigger_error = false;
        }

        $isPartial = $request->isPartial();

        $partialFields = array();
        $outputFields = array();

        if ($isPartial) {
            $partialFields = $request->getPartialRequestFields();
            $this->validatePartialFields($partialFields);
            $outputFields = $partialFields;
        } else {
            $outputFields = $this->fields;
        }

        $payload = array();

        $pidms = array();

        if (isset($options['pidm']) && $options['pidm']) {
            $this->validateInput($options['pidm'], $this->getPattern('pidm'), $this->getErrMesg('invalidPidm'));
            $pidms = $this->getValidPidm($options['pidm']);
            // $pidms = $options['pidm'];
        }

        if (isset($options['uniqueId']) && $options['uniqueId']) {
            $this->validateInput(
                $options['uniqueId'],
                $this->getPattern('uniqueId'),
                $this->getErrMesg('invalidUniqueId')
            );
            $pidms = array_merge($pidms, $this->getPidmWithUniqueId($options['uniqueId']));
        }

        if (count($pidms) < 1) {
            throw new \MiamiOH\RESTng\Exception\BadRequest("There is no IDs to search for.");
        }

        foreach ($pidms as $pidm) {
            foreach ($outputFields as $field) {
                if ($field === 'pidm') {
                    $payload[$pidm][$field] = $pidm;
                } else {
                    $payload[$pidm][$field] = '';
                }
            }
        }


        if (!$isPartial || in_array("registrationFlag", $partialFields)) {
            $regFlags = $this->getRegFlagByPidm($pidms, $options['termCode']);
            foreach ($regFlags as $regFlag) {
                $payload[$regFlag['pidm']]['registrationFlag'] = $regFlag['registration_flag'];
            }
        }

        if (!$isPartial || in_array("classCode", $partialFields) || in_array("className", $partialFields)) {
            $classes = $this->getClassByPidm($pidms, $options['termCode'], $options['mode']);

            foreach ($classes as $class) {
                if(!$isPartial || in_array("classCode", $partialFields) ) {
                    $payload[$class['pidm']]['classCode'] = $class['class_code'] ?? '';
                }
                if(!$isPartial || in_array("className", $partialFields) ) {
                    $payload[$class['pidm']]['className'] = $class['class_name'] ?? '';
                }
            }
        }

        if (isset($options['uniqueId']) && (!$isPartial || in_array("degrees", $partialFields))) {
            $pidms = $this->getPidmWithUniqueId($options['uniqueId']);
            $degrees = $this->getDegreesByUniqueId($options['uniqueId']);

            $deg = json_decode(json_encode($degrees), true);

            foreach ($pidms as $pidm) {
                $payload[$pidm]['uniqueId'] = $this->getUniqueID($pidm);
                if (!isset($deg[$pidm])) {
                    $payload[$pidm]['degrees'] = [];
                } else {
                    $payload[$pidm]['degrees'] = $deg[$pidm];
                }
            }
        }
        if (!isset($options['display']) || strtolower($options['display']) !== 'smart') {
            $temp = array();
            foreach ($payload as $record) {
                array_push($temp, $record);
            }
            $payload = $temp;
        }

        if ($payload) {
            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayLoad($payload);
        } else {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        }

        return $response;
    }

    public function getDegreesByUniqueId($uniqueIds)
    {
        $response = $this->getResponse();

        $dResponse = $this->callResource('student.academics.v1.degrees',
            array(
                'options' => array(
                    'uniqueId' => $uniqueIds
                )
            )
        );

        if ($dResponse->getStatus() !== \MiamiOH\RESTng\App::API_OK) {
            $response->setPayload(array('message' => 'Unexpected error when retrieving degrees'));
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);

            return $response;
        }
        $dPayloads = $dResponse->getPayload();

        foreach ($dPayloads as $key => $dPayload) {
            $record[$key] = $dPayload;
        }

        return $record;
    }


    private function validatePartialFields($partialFields)
    {
        if (!empty($partialFields)) {
            foreach ($partialFields as $field) {
                if (!in_array($field, $this->fields)) {
                    throw new \MiamiOH\RESTng\Exception\BadRequest($this->getErrMesg('invalidField'));
                }
            }
        }
    }

    private function getErrMesg($val)
    {
        $errMesg = array(
            'invalidPidm' => 'PIDM must be numeric and have less than or equal to 8 digits.',
            'invalidUniqueId' => 'Unique ID must be alphanumeric, and have less than or equal to 8 letters and digits.',
            'invalidField' => 'Provided fields are not valid.',
            'invalidTermCode' => 'Provided term code is not valid. Please check the term code'
        );

        return $errMesg[$val];
    }

    private function getPattern($val)
    {
        $pattern = array(
            'pidm' => '/^\d{1,8}$/',
            'uniqueId' => '/^\w{1,8}$/',
            'termCode' => '/^[0-9]+$/'
        );

        return $pattern[$val];
    }

    private function validateInput($input, $pattern, $errMesg)
    {
        if (is_array($input)) {
            foreach ($input as $value) {
                if (!preg_match($pattern, $value)) {
                    throw new \MiamiOH\RESTng\Exception\BadRequest($errMesg);
                }
            }
        } else {
            if (!preg_match($pattern, $input)) {
                throw new \MiamiOH\RESTng\Exception\BadRequest($errMesg);
            }
        }
    }

    public function getValidPidm($pidms)
    {
        if (!$this->dbh) {
            $this->dbh = $this->database->getHandle($this->datasource_name);
        }

        $records = $this->dbh->queryall_array(
            "SELECT spriden_pidm FROM spriden WHERE spriden_change_ind IS NULL AND spriden_pidm IN (?" .
            str_repeat(",?", count($pidms) - 1) . ")",
            $pidms
        );

        $pidms = array_column($records, 'spriden_pidm');

        return $pidms;
    }

    public function getPidmWithUniqueId($uniqueIds)
    {
        if (!$this->dbh) {
            $this->dbh = $this->database->getHandle($this->datasource_name);
        }

        $uniqueIds = array_map(function ($val) {
            return strtoupper(trim($val));
        }, $uniqueIds);

        $records = $this->dbh->queryall_array(
            "SELECT szbuniq_pidm FROM szbuniq WHERE szbuniq_unique_id IN (?" .
            str_repeat(",?", count($uniqueIds) - 1) . ")",
            $uniqueIds
        );

        $pidms = array_column($records, 'szbuniq_pidm');

        return $pidms;
    }

    public function getUniqueID($pidm)
    {
        $unique = $this->dbh->queryfirstcolumn(
            'select szbuniq_unique_id from szbuniq where szbuniq_pidm = ?', $pidm);

        return $unique;
    }

    public function getRegFlagByPidm($pidms, $termCode, $format = '')
    {
        $currentTerm = '';

        if (strtolower($termCode) === 'max' || strtolower($termCode) === 'current') {
            $currentTerm = "fz_get_term('N', 'S')";
        } elseif (preg_match('/^\d{6}$/', $termCode)) {
            $currentTerm = $termCode;
        } else {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Term Code is not valid');
        }

        $records = $this->dbh->queryall_array(
            "SELECT sfbetrm_pidm AS pidm, sfbetrm_term_code, sfbetrm_ests_code AS registration_flag " .
            "FROM sfbetrm " .
            "WHERE sfbetrm_term_code = $currentTerm " .
            "AND sfbetrm_pidm IN (?" . str_repeat(",?", count($pidms) - 1) . ")",
            $pidms
        );


        // mode for mutiple pidms query
        if ($format === 'smart') {
            $smartResults = array();

            foreach ($records as $record) {
                $smartResults[$record['pidm']] = $record;
            }
            $records = $smartResults;
        }

        return $records;
    }

    protected function getPidm($uniqueId)
    {
        $pidm = $this->dbh->queryfirstcolumn('
	      select szbuniq_pidm from szbuniq where upper(szbuniq_unique_id) = upper(?)
	      ', $uniqueId);

        return $pidm;
    }

    private function validateMUIDByType($idType, $id)
    {
        switch ($idType) {
            case 'uniqueId':
                $this->validateInput($id, $this->getPattern('uniqueId'), $this->getErrMesg('invalidUniqueId'));
                break;
            case 'pidm':
                $this->validateInput($id, $this->getPattern('pidm'), $this->getErrMesg('invalidPidm'));
                break;
        }
    }


    // For waitlist project
    // Check student is able to register class for the term or not
    public function checkTimeTicketByUniqueIdOrPidm()
    {
        $currentDate = new DateTime();
        $currentTimeStamp = $currentDate->getTimestamp();
        if (!$this->dbh) {
            $this->dbh = $this->database->getHandle($this->datasource_name);
        }

        //Need to validate termCode

        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();


        //get the :muid param
        $idType = $request->getResourceParamKey('muid');
        $muIdValue = $request->getResourceParam('muid');

//            validate the :muid param
        $this->validateMUIDByType($idType, $muIdValue);

        //get user pidms
        if ($idType == 'pidm') {
            $pidm = $muIdValue;
        } else {

            $person = $this->bannerUtil->getId($idType, $muIdValue);
            $pidm = $person->getPidm();
        }
        if (isset($options['termCode'])) {
            $termCode = $options['termCode'];
            $this->validateInput($termCode, $this->getPattern('termCode'),
                $this->getErrMesg('invalidTermCode'));

        } else {
            $termCode = $this->callResource('academicTerm.v2.current')->getPayload()['termId'];
        }

        $query = "select SFRWCTL.sfrwctl_term_code AS termCode, SFRWCTL.sfrwctl_priority AS priority,
                        SFRWCTL.sfrwctl_seq_no AS sequenceNo, TO_CHAR(SFRWCTL.sfrwctl_begin_date, 'DD-MON-YY') AS beginDate,
                        TO_CHAR(SFRWCTL.sfrwctl_end_date, 'DD-MON-YY')  AS endDate, SFRWCTL.sfrwctl_hour_begin as beginHour, 
                        SFRWCTL.sfrwctl_hour_end AS endHour, SFBRGRP.SFBRGRP_RGRP_CODE AS registrationgroup
                        from SFBRGRP
                        inner join SFBWCTL on SFBRGRP.SFBRGRP_RGRP_CODE = SFBWCTL.SFBWCTL_RGRP_CODE  
                        inner join SFRWCTL on SFRWCTL.SFRWCTL_PRIORITY = SFBWCTL.SFBWCTL_PRIORITY
                        where SFBRGRP.SFBRGRP_TERM_CODE =  ?
                            and SFBWCTL.SFBWCTL_TERM_CODE = ?
                            and SFRWCTL.SFRWCTL_TERM_CODE = ?
                            and SFBRGRP.SFBRGRP_PIDM=?
                            order by SFRWCTL.SFRWCTL_SEQ_NO";
        $records = $this->dbh->queryall_array($query, $termCode, $termCode, $termCode, $pidm);
        $payload = array();
        $payload['pidm'] = $pidm;
        $payload['termCode'] = $termCode;
        $payload['priority'] = '';
        $payload['registrationGroup'] = '';
        $payload['hasTimeTicket'] = 'N';
        $payload['allowedToRegister'] = 'N';
        $payload['registrationTimes'] = [];

        if (count($records) >= 1) {
            foreach ($records as $record) {
                $payload['registrationGroup'] = $record['registrationgroup'];
                $payload['hasTimeTicket'] = 'Y';
                $payload['priority'] = $record['priority'];
                $begin = $record['begindate'] . $record['beginhour'];
                $end = $record['enddate'] . $record['endhour'];
                $beginTime = $this->getTimeStampForDateAndTime($begin);
                $endTime = $this->getTimeStampForDateAndTime($end);
                if (($currentTimeStamp >= $beginTime) && ($currentTimeStamp <= $endTime)) {
                    $payload['allowedToRegister'] = 'Y';
                }
                $registrationTime = [];
                $registrationTime['sequenceNo'] = $record['sequenceno'];
                $registrationTime['beginDate'] = $record['begindate'];
                $registrationTime['endDate'] = $record['enddate'];
                $registrationTime['beginHour'] = $record['beginhour'];
                $registrationTime['endHour'] = $record['endhour'];
                $payload['registrationTimes'][] = $registrationTime;
            }
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    private function getTimeStampForDateAndTime($time)
    {
        $timeStamp = DateTime::createFromFormat('d-M-yHi', $time)->getTimestamp();

        return $timeStamp;
    }

    public function getClassByPidm($pidms, $termCode, $mode, $format = '')
    {
        $sqlWhichTerm = ''; // handle subquery for Max, Current and 6-digits termCode
        $currentTerm = ''; // varible hold current term 6 digits

        if (strtolower($termCode) === 'max') {
            $currentTerm = "fz_get_term('N', 'S')";
        } elseif (strtolower($termCode) === 'current') {
            $currentTerm = "fz_get_term('N', 'S')";
            $sqlWhichTerm = " AND sorlcur_term_code <= $currentTerm";
        } elseif (preg_match('/^\d{6}$/', $termCode)) {
            $currentTerm = $termCode;
            $sqlWhichTerm = " AND sorlcur_term_code <= " . $currentTerm;
        } else {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Term Code is not valid');
        }

        $sqlTermCode =
            '(SELECT MAX(sorlcur_term_code) ' .
            'FROM sorlcur ' .
            'WHERE sorlcur_pidm = outer_.sorlcur_pidm ' .
            "AND sorlcur_lmod_code = '" . strtoupper($mode) . "' " .
            "AND sorlcur_cact_code = 'ACTIVE'" .
            $sqlWhichTerm . ')';

        $records = $this->dbh->queryall_array(
            "SELECT pidm, fz_class_calc(pidm, level_code, $currentTerm, '') AS class_code, " .
            "stvclas_desc as class_name ".
            "FROM ( " .
            "SELECT sorlcur_pidm as pidm, nvl(sorlcur_levl_code , '') AS level_code " .
            "FROM sorlcur outer_ " .
            "WHERE sorlcur_pidm in (?" . str_repeat(",?", count($pidms) - 1) . ") " .
            "AND sorlcur_lmod_code = '" . strtoupper($mode) . "' " .
            "AND sorlcur_cact_code = 'ACTIVE' " .
            "AND sorlcur_term_code = " . $sqlTermCode . ")
            LEFT JOIN stvclas
            ON fz_class_calc(pidm, level_code, $currentTerm, '') = stvclas_code",
            $pidms
        );

        if ($format === 'smart') {
            $smartResults = array();

            foreach ($records as $record) {
                $smartResults[$record['pidm']] = $record;
            }
            $records = $smartResults;
        }

        return $records;
    }

    private function getConditionalAdminProgramCode($uniqueId)
    {
        if (!$this->dbh) {
            $this->dbh = $this->database->getHandle($this->datasource_name);
        }

        $attribute = $this->dbh->queryfirstcolumn(
            "SELECT sgrsatt_atts_code
        FROM sgrsatt, szbuniq
        WHERE szbuniq_unique_id = ?
          AND szbuniq_pidm = sgrsatt_pidm
          AND sgrsatt_atts_code IN ('ACE', 'RELC')
          AND sgrsatt_term_code_eff = (
            SELECT max(sgrsatt_term_code_eff)
              FROM sgrsatt
             WHERE sgrsatt_pidm = szbuniq_pidm)",
            strtoupper($uniqueId));
        if ($attribute == 'ACE') {
            return 'ACE';
        } else {
            if ($attribute == 'RELC') {
                return 'ELC4';
            } else {
                $elc13 = $this->dbh->queryfirstcolumn(
                    "SELECT sorlcur_pidm
          FROM sorlcur, szbuniq
         WHERE szbuniq_unique_id = ?
           and szbuniq_pidm = sorlcur_pidm
           AND sorlcur_lmod_code = 'LEARNER'
           AND sorlcur_cact_code = 'ACTIVE'
           AND sorlcur_degc_code = 999
           AND sorlcur_levl_code = 'CR'
           AND sorlcur_admt_code = 'RI'
           AND sorlcur_coll_code = 'NC'
           AND sorlcur_term_code =
             (SELECT MAX(sorlcur_term_code)
                FROM sorlcur
               WHERE sorlcur_pidm = szbuniq_pidm)",
                    strtoupper($uniqueId));
                if ($elc13 !== DB_EMPTY_SET) {
                    return 'ELC1-3';
                } else {
                    return '';
                }
            }
        }
    }

    private function updateConditionalAdmitProgramCode($uniqueId, $code)
    {
        if (!$this->dbh) {
            $this->dbh = $this->database->getHandle($this->datasource_name);
        }

        switch ($code) {
            case 'ACE':
                $sql = 'BEGIN baninst1.PZ_ACE_Load(:uniqueId); end;';
                break;
            case 'ELC4':
                $sql = 'BEGIN baninst1.PZ_ELC4_Load(:uniqueId); end;';
                break;
            case 'ELC5':
                $sql = 'BEGIN baninst1.PZ_ELC5_Load(:uniqueId); end;';
                break;
        }

        $sth = $this->dbh->prepare($sql);

        $sth->bind_by_name(":uniqueId", $uniqueId, -1);

        $sth->execute();
    }

    public function updateStudent()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $data = $request->getData();

        $uniqueId = $request->getResourceParam('uniqueId');

        $currentConditionalAdmitProgramCode = $this->getConditionalAdminProgramCode($uniqueId);
        $newConditionalAdmitProgramCode = $data['conditionalAdmitProgramCode'];

        if ($currentConditionalAdmitProgramCode == 'ACE' &&
            $newConditionalAdmitProgramCode == ''
        ) {
            $this->updateConditionalAdmitProgramCode($uniqueId, 'ACE');
        } else {
            if ($currentConditionalAdmitProgramCode == 'ELC1-3' &&
                $newConditionalAdmitProgramCode == 'ELC4'
            ) {
                $this->updateConditionalAdmitProgramCode($uniqueId, 'ELC4');
            } else {
                if (($currentConditionalAdmitProgramCode == 'ELC1-3' ||
                        $currentConditionalAdmitProgramCode == 'ELC4') &&
                    $newConditionalAdmitProgramCode == ''
                ) {
                    $this->updateConditionalAdmitProgramCode($uniqueId, 'ELC5');
                } else {
                    $errorMsg = sprintf('Invalid initial student state for program change - %s to %s',
                        ($currentConditionalAdmitProgramCode == '' ? 'Normal' : $currentConditionalAdmitProgramCode),
                        ($newConditionalAdmitProgramCode == '' ? 'Normal' : $newConditionalAdmitProgramCode));
                    $response->setPayload(array('message' => $errorMsg));
                    $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);

                    return $response;
                }
            }
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        return $response;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }
}
