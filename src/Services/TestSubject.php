<?php

namespace MiamiOH\RestngAcademics\Services;

class TestSubject extends \MiamiOH\RESTng\Service
{

    public function getSomething()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $id = $request->getResourceParam('id');

        $response->setPayload(array('id' => $id, 'name' => 'Something'));

        return $response;
    }
}