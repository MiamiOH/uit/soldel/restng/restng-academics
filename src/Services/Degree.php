<?php

namespace MiamiOH\RestngAcademics\Services;

use MiamiOH\RESTng\Exception\BadRequest;

class Degree extends \MiamiOH\RESTng\Service
{

    private $datasource_name = 'MUWS_GEN_PROD';
    private $dbh = '';

    public function getDegree()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $uniqueId = $request->getResourceParam('uniqueId');

        $options = $request->getOptions();

        if (!isset($options['mode'])) {
            $options['mode'] = 'LEARNER';
        }
        if (isset($options['termCode']) &&
            $options['termCode'] != 'current' &&
            $options['termCode'] != 'max' &&
            !preg_match('/^\d\d\d\d\d\d$/', $options['termCode'])) {
            $response->setPayload(array('message' => 'Invalid termCode option'));
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            return $response;
        }

        $authUser = $this->getApiUser()->getUsername();

        if (strtolower($authUser) != strtolower($uniqueId)) {
            if (!$this->getApiUser()->isAuthorized('WebServices', 'StudentDegree', 'view') &&
                !$this->getApiUser()->isAuthorized('WebServices', 'StudentDegree', 'All')) {
                $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
                return $response;
            }
        }

        $dbh = $this->database->getHandle($this->datasource_name);
        $dbh->mu_trigger_error = false;

        $pidm = $dbh->queryfirstcolumn("SELECT szbuniq_pidm FROM szbuniq WHERE szbuniq_unique_id = ?", strtoupper($uniqueId));
        if ($pidm === DB_EMPTY_SET) {
            $response->setPayload(array('message' => 'User not found'));
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        if (!isset($options['termCode']) || $options['termCode'] == 'current') {
            $degrees = $dbh->queryall_array(
                "SELECT sorlcur_seqno                AS id,
                nvl(sorlcur_levl_code , ' ') AS level_code,
                nvl(stvlevl_desc      , ' ') AS level_name,
                nvl(sorlcur_degc_code , ' ') AS degree_code,
                nvl(stvdegc_desc      , ' ') AS degree_name,
                nvl(sorlcur_coll_code , ' ') AS college_code,
                nvl(stvcoll_desc      , ' ') AS college_name,
                nvl(sorlcur_camp_code , ' ') AS campus_code,
                nvl(stvcamp_desc      , ' ') AS campus_name,
                nvl(sorlcur_styp_code , ' ') AS type_code,
                nvl(stvstyp_desc      , ' ') AS type_name,
                nvl(sorlcur_admt_code , ' ') AS admit_type_code,
                nvl(stvadmt_desc      , ' ') AS admit_type_name,
                nvl(sorlcur_program   , ' ') AS program_code
           FROM sorlcur
      LEFT JOIN stvcamp
             ON sorlcur_camp_code = stvcamp_code
      LEFT JOIN stvclas
             ON stvclas_code = fz_class_calc(?, sorlcur_levl_code, fz_get_term(), '')
      LEFT JOIN stvdegc
             ON stvdegc_code = sorlcur_degc_code
      LEFT JOIN stvcoll
             ON stvcoll_code = sorlcur_coll_code
      LEFT JOIN stvstyp
             ON stvstyp_code = sorlcur_styp_code
      LEFT JOIN stvlevl
             ON sorlcur_levl_code = stvlevl_code
            AND stvlevl_system_req_ind IS NULL
      LEFT JOIN stvadmt
             ON stvadmt_code = sorlcur_admt_code
          WHERE sorlcur_pidm      = ?
            AND sorlcur_lmod_code = ?
            AND sorlcur_cact_code = 'ACTIVE'
            AND sorlcur_term_code =
              (SELECT MAX(sorlcur_term_code)
                 FROM sorlcur
                WHERE sorlcur_pidm       = ?
                  AND sorlcur_term_code <= fz_get_term())
            ORDER BY sorlcur_priority_no",
                $pidm,
                $pidm,
                $options['mode'],
                $pidm);
        } else if ($options['termCode'] == 'max') {
            $degrees = $dbh->queryall_array(
                "SELECT sorlcur_seqno                AS id,
                nvl(sorlcur_levl_code , ' ') AS level_code,
                nvl(stvlevl_desc      , ' ') AS level_name,
                nvl(sorlcur_degc_code , ' ') AS degree_code,
                nvl(stvdegc_desc      , ' ') AS degree_name,
                nvl(sorlcur_coll_code , ' ') AS college_code,
                nvl(stvcoll_desc      , ' ') AS college_name,
                nvl(sorlcur_camp_code , ' ') AS campus_code,
                nvl(stvcamp_desc      , ' ') AS campus_name,
                nvl(sorlcur_styp_code , ' ') AS type_code,
                nvl(stvstyp_desc      , ' ') AS type_name,
                nvl(sorlcur_admt_code , ' ') AS admit_type_code,
                nvl(stvadmt_desc      , ' ') AS admit_type_name,
                nvl(sorlcur_program   , ' ') AS program_code
           FROM sorlcur
      LEFT JOIN stvcamp
             ON sorlcur_camp_code = stvcamp_code
      LEFT JOIN stvclas
             ON stvclas_code = fz_class_calc(?, sorlcur_levl_code, fz_get_term(), '')
      LEFT JOIN stvdegc
             ON stvdegc_code = sorlcur_degc_code
      LEFT JOIN stvcoll
             ON stvcoll_code = sorlcur_coll_code
      LEFT JOIN stvstyp
             ON stvstyp_code = sorlcur_styp_code
      LEFT JOIN stvlevl
             ON sorlcur_levl_code = stvlevl_code
            AND stvlevl_system_req_ind IS NULL
      LEFT JOIN stvadmt
             ON stvadmt_code = sorlcur_admt_code
          WHERE sorlcur_pidm      = ?
            AND sorlcur_lmod_code = ?
            AND sorlcur_cact_code = 'ACTIVE'
            AND sorlcur_term_code =
              (SELECT MAX(sorlcur_term_code)
                 FROM sorlcur
                WHERE sorlcur_pidm       = ?)
            ORDER BY sorlcur_priority_no",
                $pidm,
                $pidm,
                $options['mode'],
                $pidm);
        } else {
            $degrees = $dbh->queryall_array(
                "SELECT sorlcur_seqno                AS id,
                nvl(sorlcur_levl_code , ' ') AS level_code,
                nvl(stvlevl_desc      , ' ') AS level_name,
                nvl(sorlcur_degc_code , ' ') AS degree_code,
                nvl(stvdegc_desc      , ' ') AS degree_name,
                nvl(sorlcur_coll_code , ' ') AS college_code,
                nvl(stvcoll_desc      , ' ') AS college_name,
                nvl(sorlcur_camp_code , ' ') AS campus_code,
                nvl(stvcamp_desc      , ' ') AS campus_name,
                nvl(sorlcur_styp_code , ' ') AS type_code,
                nvl(stvstyp_desc      , ' ') AS type_name,
                nvl(sorlcur_admt_code , ' ') AS admit_type_code,
                nvl(stvadmt_desc      , ' ') AS admit_type_name,
                nvl(sorlcur_program   , ' ') AS program_code
           FROM sorlcur
      LEFT JOIN stvcamp
             ON sorlcur_camp_code = stvcamp_code
      LEFT JOIN stvclas
             ON stvclas_code = fz_class_calc(?, sorlcur_levl_code, ?, '')
      LEFT JOIN stvdegc
             ON stvdegc_code = sorlcur_degc_code
      LEFT JOIN stvcoll
             ON stvcoll_code = sorlcur_coll_code
      LEFT JOIN stvstyp
             ON stvstyp_code = sorlcur_styp_code
      LEFT JOIN stvlevl
             ON sorlcur_levl_code = stvlevl_code
            AND stvlevl_system_req_ind IS NULL
      LEFT JOIN stvadmt
             ON stvadmt_code = sorlcur_admt_code
          WHERE sorlcur_pidm      = ?
            AND sorlcur_lmod_code = ?
            AND sorlcur_cact_code = 'ACTIVE'
            AND sorlcur_term_code =
              (SELECT MAX(sorlcur_term_code)
                 FROM sorlcur
                WHERE sorlcur_pidm       = ?
                  AND sorlcur_term_code <= ?)
            ORDER BY sorlcur_priority_no",
                $pidm,
                $options['termCode'],
                $pidm,
                $options['mode'],
                $pidm,
                $options['termCode']);
        }

        $returnArray = Array();
        $count = -1;
        foreach ($degrees as $degree) {
            $count++;
            $returnArray[$count]['id'] = $degree['id'];
            $returnArray[$count]['levelCode'] = trim($degree['level_code']);
            $returnArray[$count]['levelName'] = trim($degree['level_name']);
            $returnArray[$count]['degreeCode'] = trim($degree['degree_code']);
            $returnArray[$count]['degreeName'] = trim($degree['degree_name']);
            $returnArray[$count]['collegeCode'] = trim($degree['college_code']);
            $returnArray[$count]['collegeName'] = trim($degree['college_name']);
            $returnArray[$count]['campusCode'] = trim($degree['campus_code']);
            $returnArray[$count]['campusName'] = trim($degree['campus_name']);
            $returnArray[$count]['typeCode'] = trim($degree['type_code']);
            $returnArray[$count]['typeName'] = trim($degree['type_name']);
            $returnArray[$count]['admitTypeCode'] = trim($degree['admit_type_code']);
            $returnArray[$count]['admitTypeName'] = trim($degree['admit_type_name']);
            $returnArray[$count]['programCode'] = trim($degree['program_code']);

            $fosResponse = $this->callResource('student.academics.v1.fieldOfStudy',
                array('params' => array('uniqueId' => $uniqueId,
                    'degId' => $degree['id'])));
            if ($fosResponse->getStatus() !== \MiamiOH\RESTng\App::API_OK) {
                $response->setPayload(array('message' => 'User not found when retrieving field of study'));
                $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
                return $response;
            }
            $returnArray[$count]['fieldOfStudy'] = $fosResponse->getPayload();
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($returnArray);

        return $response;
    }

    //
    public function getDegrees()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        if (!isset($options['mode'])) {
            $options['mode'] = 'LEARNER';
        }
        if (isset($options['termCode']) &&
            $options['termCode'] != 'current' &&
            $options['termCode'] != 'max' &&
            !preg_match('/^\d\d\d\d\d\d$/', $options['termCode'])) {
            $response->setPayload(array('message' => 'Invalid termCode option'));
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            return $response;
        }

        $authUser = $this->getApiUser()->getUsername();

        if (!$this->getApiUser()->isAuthorized('WebServices', 'StudentDegree', 'view') &&
            !$this->getApiUser()->isAuthorized('WebServices', 'StudentDegree', 'All')) {
            $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
            return $response;
        }


        $dbh = $this->database->getHandle($this->datasource_name);
        $dbh->mu_trigger_error = false;
        $pidms = [];
        if (isset($options['uniqueId']) && $options['uniqueId']) {
            $this->validateInput(
                $options['uniqueId'],
                $this->getPattern('uniqueId'),
                $this->getErrMesg('invalidUniqueId')
            );
            $pidms = array_merge($pidms, $this->getPidmWithUniqueId($options['uniqueId']));
        }


        if (count($pidms) < 1) {
            throw new \MiamiOH\RESTng\Exception\BadRequest("There is no IDs to search for.");
        }

        $stringPidms = implode(",", $pidms);

        if (!isset($options['termCode']) || $options['termCode'] == 'current') {
            $degrees = $dbh->queryall_array(
                "SELECT
                sorlcur_pidm                 AS pidm,
                sorlcur_seqno                AS id,
                nvl(sorlcur_levl_code , ' ') AS level_code,
                nvl(stvlevl_desc      , ' ') AS level_name,
                nvl(sorlcur_degc_code , ' ') AS degree_code,
                nvl(stvdegc_desc      , ' ') AS degree_name,
                nvl(sorlcur_coll_code , ' ') AS college_code,
                nvl(stvcoll_desc      , ' ') AS college_name,
                nvl(sorlcur_camp_code , ' ') AS campus_code,
                nvl(stvcamp_desc      , ' ') AS campus_name,
                nvl(sorlcur_styp_code , ' ') AS type_code,
                nvl(stvstyp_desc      , ' ') AS type_name,
                nvl(sorlcur_admt_code , ' ') AS admit_type_code,
                nvl(stvadmt_desc      , ' ') AS admit_type_name,
                nvl(sorlcur_program   , ' ') AS program_code
           FROM sorlcur a
      LEFT JOIN stvcamp
             ON sorlcur_camp_code = stvcamp_code
      LEFT JOIN stvclas
             ON stvclas_code = fz_class_calc(a.sorlcur_pidm, sorlcur_levl_code, fz_get_term(), '')
      LEFT JOIN stvdegc
             ON stvdegc_code = sorlcur_degc_code
      LEFT JOIN stvcoll
             ON stvcoll_code = sorlcur_coll_code
      LEFT JOIN stvstyp
             ON stvstyp_code = sorlcur_styp_code
      LEFT JOIN stvlevl
             ON sorlcur_levl_code = stvlevl_code
            AND stvlevl_system_req_ind IS NULL
      LEFT JOIN stvadmt
             ON stvadmt_code = sorlcur_admt_code
          WHERE sorlcur_pidm      IN ($stringPidms)
            AND sorlcur_lmod_code = ?
            AND sorlcur_cact_code = 'ACTIVE'
            AND sorlcur_term_code =
              (SELECT MAX(sorlcur_term_code)
                 FROM sorlcur b
                WHERE sorlcur_pidm = a.sorlcur_pidm
                  AND sorlcur_term_code <= fz_get_term())
            ORDER BY sorlcur_priority_no",
                $options['mode']);

        } else if ($options['termCode'] == 'max') {
            $degrees = $dbh->queryall_array(
                "SELECT sorlcur_seqno                AS id,
                sorlcur_pidm                 AS pidm,
                nvl(sorlcur_levl_code , ' ') AS level_code,
                nvl(stvlevl_desc      , ' ') AS level_name,
                nvl(sorlcur_degc_code , ' ') AS degree_code,
                nvl(stvdegc_desc      , ' ') AS degree_name,
                nvl(sorlcur_coll_code , ' ') AS college_code,
                nvl(stvcoll_desc      , ' ') AS college_name,
                nvl(sorlcur_camp_code , ' ') AS campus_code,
                nvl(stvcamp_desc      , ' ') AS campus_name,
                nvl(sorlcur_styp_code , ' ') AS type_code,
                nvl(stvstyp_desc      , ' ') AS type_name,
                nvl(sorlcur_admt_code , ' ') AS admit_type_code,
                nvl(stvadmt_desc      , ' ') AS admit_type_name,
                nvl(sorlcur_program   , ' ') AS program_code
           FROM sorlcur a
      LEFT JOIN stvcamp
             ON sorlcur_camp_code = stvcamp_code
      LEFT JOIN stvclas
             ON stvclas_code = fz_class_calc(a.sorlcur_pidm, sorlcur_levl_code, fz_get_term(), '')
      LEFT JOIN stvdegc
             ON stvdegc_code = sorlcur_degc_code
      LEFT JOIN stvcoll
             ON stvcoll_code = sorlcur_coll_code
      LEFT JOIN stvstyp
             ON stvstyp_code = sorlcur_styp_code
      LEFT JOIN stvlevl
             ON sorlcur_levl_code = stvlevl_code
            AND stvlevl_system_req_ind IS NULL
      LEFT JOIN stvadmt
             ON stvadmt_code = sorlcur_admt_code
          WHERE sorlcur_pidm      IN ($stringPidms)
            AND sorlcur_lmod_code = ?
            AND sorlcur_cact_code = 'ACTIVE'
            AND sorlcur_term_code =
              (SELECT MAX(sorlcur_term_code)
                 FROM sorlcur b
                WHERE sorlcur_pidm       = a.sorlcur_pidm)
            ORDER BY sorlcur_priority_no",
                $options['mode']);
        } else {
            $degrees = $dbh->queryall_array(
                "SELECT sorlcur_seqno                AS id,
                sorlcur_pidm                 AS pidm,
                nvl(sorlcur_levl_code , ' ') AS level_code,
                nvl(stvlevl_desc      , ' ') AS level_name,
                nvl(sorlcur_degc_code , ' ') AS degree_code,
                nvl(stvdegc_desc      , ' ') AS degree_name,
                nvl(sorlcur_coll_code , ' ') AS college_code,
                nvl(stvcoll_desc      , ' ') AS college_name,
                nvl(sorlcur_camp_code , ' ') AS campus_code,
                nvl(stvcamp_desc      , ' ') AS campus_name,
                nvl(sorlcur_styp_code , ' ') AS type_code,
                nvl(stvstyp_desc      , ' ') AS type_name,
                nvl(sorlcur_admt_code , ' ') AS admit_type_code,
                nvl(stvadmt_desc      , ' ') AS admit_type_name,
                nvl(sorlcur_program   , ' ') AS program_code
           FROM sorlcur a
      LEFT JOIN stvcamp
             ON sorlcur_camp_code = stvcamp_code
      LEFT JOIN stvclas
             ON stvclas_code = fz_class_calc(a.sorlcur_pidm, sorlcur_levl_code, ?, '')
      LEFT JOIN stvdegc
             ON stvdegc_code = sorlcur_degc_code
      LEFT JOIN stvcoll
             ON stvcoll_code = sorlcur_coll_code
      LEFT JOIN stvstyp
             ON stvstyp_code = sorlcur_styp_code
      LEFT JOIN stvlevl
             ON sorlcur_levl_code = stvlevl_code
            AND stvlevl_system_req_ind IS NULL
      LEFT JOIN stvadmt
             ON stvadmt_code = sorlcur_admt_code
          WHERE sorlcur_pidm     IN ($stringPidms)
            AND sorlcur_lmod_code = ?
            AND sorlcur_cact_code = 'ACTIVE'
            AND sorlcur_term_code =
              (SELECT MAX(sorlcur_term_code)
                 FROM sorlcur b
                WHERE sorlcur_pidm       = a.sorlcur_pidm
                  AND sorlcur_term_code <= ?)
            ORDER BY sorlcur_priority_no",
                $options['termCode'],
                $options['mode'],
                $options['termCode']);
        }

        $returnArray = Array();
        $count = -1;
        foreach ($degrees as $degree) {
            $count++;
            $returnArray[$count][$degree['pidm']]['id'] = $degree['id'];
            $returnArray[$count][$degree['pidm']]['levelCode'] = trim($degree['level_code']);
            $returnArray[$count][$degree['pidm']]['levelName'] = trim($degree['level_name']);
            $returnArray[$count][$degree['pidm']]['degreeCode'] = trim($degree['degree_code']);
            $returnArray[$count][$degree['pidm']]['degreeName'] = trim($degree['degree_name']);
            $returnArray[$count][$degree['pidm']]['collegeCode'] = trim($degree['college_code']);
            $returnArray[$count][$degree['pidm']]['collegeName'] = trim($degree['college_name']);
            $returnArray[$count][$degree['pidm']]['campusCode'] = trim($degree['campus_code']);
            $returnArray[$count][$degree['pidm']]['campusName'] = trim($degree['campus_name']);
            $returnArray[$count][$degree['pidm']]['typeCode'] = trim($degree['type_code']);
            $returnArray[$count][$degree['pidm']]['typeName'] = trim($degree['type_name']);
            $returnArray[$count][$degree['pidm']]['admitTypeCode'] = trim($degree['admit_type_code']);
            $returnArray[$count][$degree['pidm']]['admitTypeName'] = trim($degree['admit_type_name']);
            $returnArray[$count][$degree['pidm']]['programCode'] = trim($degree['program_code']);

            $fosResponse = $this->callResource('student.academics.v1.fieldOfStudy',
                array('params' => array('uniqueId' => $this->getUniqueID($degree['pidm']),
                    'degId' => $degree['id'])));
            if ($fosResponse->getStatus() !== \MiamiOH\RESTng\App::API_OK) {
                $response->setPayload(array('message' => 'User not found when retrieving field of study'));
                $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
                return $response;
            }
            $returnArray[$count][$degree['pidm']]['fieldOfStudy'] = $fosResponse->getPayload();
        }

        $result = [];
        foreach ($returnArray as $items) {
            foreach ($items as $id => $item) {
                $result[$id][] = $item;
            }
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($result);

        return $response;

    }

    private function getErrMesg($val)
    {
        $errMesg = array(
            'invalidPidm' => 'PIDM must be numeric and have less than or equal to 8 digits.',
            'invalidUniqueId' => 'Unique ID must be alphanumeric, and have less than or equal to 8 letters and digits.',
            'invalidField' => 'Provided fields are not valid.'
        );
        return $errMesg[$val];
    }

    private function getPattern($val)
    {
        $pattern = array(
            'pidm' => '/^\d{1,8}$/',
            'uniqueId' => '/^\w{1,8}$/'
        );
        return $pattern[$val];
    }

    private function validateInput($input, $pattern, $errMesg)
    {
        if (is_array($input)) {
            foreach ($input as $value) {
                if (!preg_match($pattern, $value)) {
                    throw new \MiamiOH\RESTng\Exception\BadRequest($errMesg);
                }
            }
        } else {
            if (!preg_match($pattern, $input)) {
                throw new \MiamiOH\RESTng\Exception\BadRequest($errMesg);
            }
        }
    }

    public function getPidmWithUniqueId($uniqueIds)
    {
        if (!$this->dbh) {
            $this->dbh = $this->database->getHandle($this->datasource_name);
        }

        $uniqueIds = array_map(function ($val) {
            return strtoupper(trim($val));
        }, $uniqueIds);

        $records = $this->dbh->queryall_array(
            "SELECT szbuniq_pidm FROM szbuniq WHERE szbuniq_unique_id IN (?" .
            str_repeat(",?", count($uniqueIds) - 1) . ")",
            $uniqueIds
        );

        $pidms = array_column($records, 'szbuniq_pidm');

        return $pidms;
    }

    protected function getUniqueID($pidm)
    {
        $unique = $this->dbh->queryfirstcolumn(
            'select szbuniq_unique_id from szbuniq where szbuniq_pidm = ?', $pidm);
        return $unique;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }
}
