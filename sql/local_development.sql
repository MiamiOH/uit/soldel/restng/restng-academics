grant MUWS_GEN_RL to saturn;
grant MUWS_SEC_RL to saturn;

grant delete on job_queue_list to saturn;
grant delete on job_queue_detail to saturn;
grant delete on job_queue_run to saturn;
grant delete on job_queue_out to saturn;
grant delete on stu_master to saturn;
grant delete on stu_demo to saturn;