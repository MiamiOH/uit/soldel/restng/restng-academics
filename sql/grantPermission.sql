GRANT SELECT ON sfbetrm TO MUWS_GEN_RL;

grant select on job_queue_list to MUWS_GEN_RL;
grant insert on job_queue_list to MUWS_GEN_RL;
grant update on job_queue_list to MUWS_GEN_RL;

grant select on job_queue_detail to MUWS_GEN_RL;
grant insert on job_queue_detail to MUWS_GEN_RL;
grant update on job_queue_detail to MUWS_GEN_RL;

grant select on job_queue_run to MUWS_GEN_RL;
grant insert on job_queue_run to MUWS_GEN_RL;
grant update on job_queue_run to MUWS_GEN_RL;

grant select on job_queue_out to MUWS_GEN_RL;
grant insert on job_queue_out to MUWS_GEN_RL;
grant update on job_queue_out to MUWS_GEN_RL;

grant select on stu_master to MUWS_GEN_RL;
grant insert on stu_master to MUWS_GEN_RL;
grant update on stu_master to MUWS_GEN_RL;

grant select on stu_demo to MUWS_GEN_RL;
grant insert on stu_demo to MUWS_GEN_RL;
grant update on stu_demo to MUWS_GEN_RL;



grant select on SFBRGRP to MUWS_GEN_RL;
grant select on SFRWCTL to MUWS_GEN_RL;
grant select on SFBWCTL to MUWS_GEN_RL;
