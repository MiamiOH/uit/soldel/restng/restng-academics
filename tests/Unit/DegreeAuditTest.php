<?php

namespace MiamiOH\RestngAcademics\Tests\Unit;

class DegreeAuditTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $api;
    private $dbh;
    private $sth;
    private $request;
    private $degreeAuditReport;
    private $bannerUtil;
    private $validator;


    protected function setUp(): void
    {

        $this->api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'callResource'))
            ->getMock();

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getResourceParamKey'))
            ->getMock();

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array', 'queryfirstcolumn', 'perform', 'commit', 'rollback'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(['getHandle'])
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();

        $this->validator = new \MiamiOH\RESTng\Util\ValidationFactory();

        $this->dbh->method('perform')->willReturn(true);
        $this->dbh->method('commit')->willReturn(true);
        $this->dbh->method('rollback')->willReturn(true);


        $this->degreeAuditReport = new \MiamiOH\RestngAcademics\Services\DegreeAudit();
        $this->degreeAuditReport->setDatabase($db);
        $this->degreeAuditReport->setBannerUtil($this->bannerUtil);
        $this->degreeAuditReport->setRequest($this->request);
        $this->degreeAuditReport->setValidation($this->validator);
    }


    /***********************
     * CREAT tests
     ********************/
    public function testCreateDARForOneUserValidUniqueID_returnsJobID()
    {
        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn('rtr1024');

        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('uniqueId');

        $person = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm', 'getUniqueId', 'getBannerId'))
            ->getMock();

        $person->method('getPidm')
            ->willReturn(101273);

        $person->method('getUniqueId')
            ->willReturn('rtr1024');

        $person->method('getBannerId')
            ->willReturn('+101273');

        $this->bannerUtil->method('getId')
            ->with('uniqueId', $this->anything())
            ->willReturn($person);

        $this->dbh->method('queryall_array')
            ->will($this->onConsecutiveCalls($this->returnCallback(array($this, 'mockQueryFullName')),
                $this->returnCallback(array($this, 'mockStudentMasterRecordExist')),
                $this->returnCallback(array($this, 'mockQueryFullName')),
                $this->returnCallback(array($this, 'mockStudentMasterRecordExist')),
                $this->returnCallback(array($this, 'mockStudentDemoRecordExist'))
            ));

        $this->response = $this->degreeAuditReport->createDARForOne();
        $this->assertEquals(\MiamiOH\RESTng\App::API_CREATED, $this->response->getStatus());
        $this->assertEquals($this->mockPostResponseSingle(), $this->response->getPayload());
    }


    public function testCreateDARForOneUserValidPidm_returnsJobID()
    {
        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn(101273);

        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('pidm');

        $person = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm', 'getUniqueId', 'getBannerId'))
            ->getMock();

        $person->method('getPidm')
            ->willReturn(101273);

        $person->method('getUniqueId')
            ->willReturn('rtr1024');

        $person->method('getBannerId')
            ->willReturn('+101273');

        $this->bannerUtil->method('getId')
            ->with('pidm', $this->anything())
            ->willReturn($person);

        $this->dbh->method('queryall_array')
            ->will($this->onConsecutiveCalls($this->returnCallback(array($this, 'mockQueryFullName')),
                $this->returnCallback(array($this, 'mockStudentMasterRecordExist')),
                $this->returnCallback(array($this, 'mockQueryFullName')),
                $this->returnCallback(array($this, 'mockStudentMasterRecordExist')),
                $this->returnCallback(array($this, 'mockStudentDemoRecordExist'))
            ));

        $this->response = $this->degreeAuditReport->createDARForOne();
        $this->assertEquals(\MiamiOH\RESTng\App::API_CREATED, $this->response->getStatus());
        $this->assertEquals($this->mockPostResponseSingle(), $this->response->getPayload());
    }

    public function testCreateDARForOneUserValidPidmNoStudentRecord_returnsJobID()
    {
        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn(101273);

        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('pidm');

        $person = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm', 'getUniqueId', 'getBannerId'))
            ->getMock();

        $person->method('getPidm')
            ->willReturn(101273);

        $person->method('getUniqueId')
            ->willReturn('rtr1024');

        $person->method('getBannerId')
            ->willReturn('+101273');

        $this->bannerUtil->method('getId')
            ->with('pidm', $this->anything())
            ->willReturn($person);

        $this->dbh->method('queryall_array')
            ->will($this->onConsecutiveCalls($this->returnCallback(array($this, 'mockQueryFullName')),
                [],
                [],
                [],
                $this->returnCallback(array($this, 'mockQueryFullName')),
                $this->returnCallback(array($this, 'mockStudentMasterRecordExist')),
                $this->returnCallback(array($this, 'mockStudentDemoRecordExist'))
            ));

        $this->response = $this->degreeAuditReport->createDARForOne();
        $this->assertEquals(\MiamiOH\RESTng\App::API_CREATED, $this->response->getStatus());
        $this->assertEquals($this->mockPostResponseSingle(), $this->response->getPayload());
    }


    public function testCreateDARForOneUserValidPidmNoFullName_returnsJobID()
    {
        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn(101273);

        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('pidm');

        $person = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm', 'getUniqueId', 'getBannerId'))
            ->getMock();

        $person->method('getPidm')
            ->willReturn(101273);

        $person->method('getUniqueId')
            ->willReturn('rtr1024');

        $person->method('getBannerId')
            ->willReturn('+101273');

        $this->bannerUtil->method('getId')
            ->with('pidm', $this->anything())
            ->willReturn($person);

        $this->dbh->method('queryall_array')
            ->will($this->onConsecutiveCalls([],
                [],
                [],
                $this->returnCallback(array($this, 'mockStudentMasterRecordExist')),
                [],
                $this->returnCallback(array($this, 'mockStudentMasterRecordExist')),
                $this->returnCallback(array($this, 'mockStudentDemoRecordExist'))
            ));

        $this->response = $this->degreeAuditReport->createDARForOne();
        $this->assertEquals(\MiamiOH\RESTng\App::API_CREATED, $this->response->getStatus());
        $this->assertEquals($this->mockPostResponseSingle(), $this->response->getPayload());
    }

    public function testCreateDARForOneUserInvalidUniqueID_returnsBadRequest()
    {
        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn('rtr1024*&(');

        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('uniqueId');

        $this->expectException(\MiamiOH\RESTng\Exception\BadRequest::class);

        $this->response = $this->degreeAuditReport->createDARForOne();

    }

    public function testCreateDARForOneUserInvalidPidm_returnsBadRequest()
    {
        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn('+3234234');

        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('pidm');

        $this->expectException(\MiamiOH\RESTng\Exception\BadRequest::class);

        $this->response = $this->degreeAuditReport->createDARForOne();

    }

    public function testCreateDARsForMultipleUsersValidUniqueIDs_returnsJobIDs()
    {

        $this->request->method('getOptions')
            ->willReturn(array('uniqueId' => ['rtr1023', 'rtr1024']));


        $person = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm', 'getUniqueId', 'getBannerId'))
            ->getMock();

        $person->method('getPidm')
            ->will($this->onConsecutiveCalls(579898, 103409));

        $person->method('getUniqueId')
            ->will($this->onConsecutiveCalls('rtr1023', 'rtr1024'));

        $person->method('getBannerId')
            ->will($this->onConsecutiveCalls('+143234', '+00103409'));

        $this->bannerUtil->method('getId')
            ->with('uniqueId', $this->anything())
            ->willReturn($person);

        $this->dbh->method('queryall_array')
            ->will($this->onConsecutiveCalls(
                $this->returnCallback(array($this, 'mockQueryFullName')),
                $this->returnCallback(array($this, 'mockStudentMasterRecordExist')),
                $this->returnCallback(array($this, 'mockQueryFullName')),
                $this->returnCallback(array($this, 'mockStudentMasterRecordExist')),
                $this->returnCallback(array($this, 'mockQueryFullName')),
                $this->returnCallback(array($this, 'mockStudentMasterRecordExist')),
                $this->returnCallback(array($this, 'mockStudentDemoRecordExist')),
                $this->returnCallback(array($this, 'mockQueryFullName')),
                $this->returnCallback(array($this, 'mockStudentMasterRecordExist')),
                $this->returnCallback(array($this, 'mockStudentDemoRecordExist'))
            ));

        $this->response = $this->degreeAuditReport->createDARForMultiples();
        $this->assertEquals(\MiamiOH\RESTng\App::API_CREATED, $this->response->getStatus());
        $this->assertEquals($this->mockPostResponseMultiple(), $this->response->getPayload());
    }

    public function testCreateDARsForMultipleUsersInvalidUniqueIDs_returnsBadRequest()
    {

        $this->request->method('getOptions')
            ->willReturn(array('uniqueId' => ['rtr1023', 'rtr1024*^']));

        $this->expectException(\MiamiOH\RESTng\Exception\BadRequest::class);

        $this->response = $this->degreeAuditReport->createDARForMultiples();

    }

    public function testCreateDARsForMultipleUsersNoUniqueIDs_returnsBadRequest()
    {

        $this->request->method('getOptions')
            ->willReturn(array());

        $this->expectException(\MiamiOH\RESTng\Exception\BadRequest::class);

        $this->response = $this->degreeAuditReport->createDARForMultiples();

    }


    /***********************
     * READ tests
     ********************/

    public function testGetDARsFromJobID_returnsDARs()
    {

        $jobID = 'WS20170713142359';

        $this->request->method('getResourceParam')
            ->with('jobId')
            ->willReturn($jobID);

        $this->dbh->method('queryall_array')
            ->will($this->onConsecutiveCalls(
                $this->returnCallback(array($this, 'mockQueryJobID')),
                $this->returnCallback(array($this, 'mockQueryAuditIDs')),
                $this->returnCallback(array($this, 'mockQueryDAROutput')),
                $this->returnCallback(array($this, 'mockQueryDAROutput')),
                $this->returnCallback(array($this, 'mockQueryDAROutput')),
                $this->returnCallback(array($this, 'mockQueryDAROutput'))
            ));

        $this->response = $this->degreeAuditReport->getDARFromJobID();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($this->mockGETFromJobIDResponse($jobID), $this->response->getPayload());

    }


    public function testGetDARsFromJobIDInvalidJobID_returnsBadRequest()
    {

        $this->request->method('getResourceParam')
            ->with('jobId')
            ->willReturn('2*24230');

        $this->expectException(\MiamiOH\RESTng\Exception\BadRequest::class);

        $this->response = $this->degreeAuditReport->getDARFromJobID();

    }

    public function testGetDARsFromJobIDInvalidAuditFormat_returnsBadRequest()
    {

        $this->request->method('getResourceParam')
            ->with('jobId')
            ->willReturn('WS201707081122');

        $this->request->method('getOptions')
            ->willReturn(array('auditFormat' => 'freestyle'));

        $this->expectException(\MiamiOH\RESTng\Exception\BadRequest::class);

        $this->response = $this->degreeAuditReport->getDARFromJobID();

    }


    public function mockQueryFullName()
    {
        return array(
            array
            (
                'fullname' => 'Manzo, Rocco'
            )
        );
    }


    public function mockStudentMasterRecordExist()
    {
        return array(
            array
            (
                'int_seq_no' => 100
            )
        );

    }

    public function mockStudentDemoRecordExist()
    {
        return array(
            array
            (
                'stuname' => 'Manzo, Rocco'
            )

        );

    }

    public function mockPostResponseSingle()
    {
        $jobID = 'WS' . date('YmdHis');
        return array(
            'studentUid' => 'rtr1024',
            'jobId' => $jobID,
            'auditResource' => '/rtr1024/' . $jobID
        );
    }

    public function mockQueryJobID()
    {
        return array(
            array(
                'jobId' => 'WS201707081122',
            )
        );

    }

    public function mockQueryAuditIDs()
    {
        return array(
            array(
                'int_seq_no' => '12345',
                'szbuniq_unique_id' => 'rtr1023',
                'degree' => 'degree_1'
            ),
            array(
                'int_seq_no' => '12347',
                'szbuniq_unique_id' => 'rtr1023',
                'degree' => 'degree_2'
            ),
            array(
                'int_seq_no' => '12348',
                'szbuniq_unique_id' => 'rtr1024',
                'degree' => 'degree_3'
            ),
            array(
                'int_seq_no' => '12350',
                'szbuniq_unique_id' => 'rtr1024',
                'degree' => 'degree_4'
            )
        );
    }

    public function mockQueryDAROutput()
    {
        return array(
            array(
                'darout' => 'DAR content',
            )
        );
    }

    public function mockPostResponseMultiple()
    {
        $jobID = 'WS' . date('YmdHis');
        return array(
            array(
                'studentUid' => 'rtr1023',
                'jobId' => $jobID,
                'auditResource' => '/rtr1023/' . $jobID
            ),
            array(
                'studentUid' => 'rtr1024',
                'jobId' => $jobID,
                'auditResource' => '/rtr1024/' . $jobID
            )
        );
    }

    public function mockGETFromJobIDResponse($jobID)
    {
        return array(
            array(
                'studentUid' => 'rtr1023',
                'jobId' => $jobID,
                'auditId' => '12345',
                'auditDegree' => 'degree_1',
                'auditStatus' => 'Completed',
                'auditFormat' => 'Double Column',
                'auditOutput' => 'DAR content',
            ),
            array(
                'studentUid' => 'rtr1023',
                'jobId' => $jobID,
                'auditId' => '12347',
                'auditDegree' => 'degree_2',
                'auditStatus' => 'Completed',
                'auditFormat' => 'Double Column',
                'auditOutput' => 'DAR content',
            ),
            array(
                'studentUid' => 'rtr1024',
                'jobId' => $jobID,
                'auditId' => '12348',
                'auditDegree' => 'degree_3',
                'auditStatus' => 'Completed',
                'auditFormat' => 'Double Column',
                'auditOutput' => 'DAR content',
            ),
            array(
                'studentUid' => 'rtr1024',
                'jobId' => $jobID,
                'auditId' => '12350',
                'auditDegree' => 'degree_4',
                'auditStatus' => 'Completed',
                'auditFormat' => 'Double Column',
                'auditOutput' => 'DAR content',
            )
        );

    }


}
