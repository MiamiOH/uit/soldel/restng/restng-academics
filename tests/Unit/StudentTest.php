<?php

namespace MiamiOH\RestngAcademics\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Response;

class StudentTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $student;
    private $api;
    private $request;
    private $dbh;
    private $sth;
    private $user;
    private $bannerUtil;

    protected function setUp(): void
    {
        //set up the mock api:
        $this->api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'getResourceParam', 'callResource'))
            ->getMock();


        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array(
                'getResourceParam',
                'getResourceParamKey',
                'getOptions',
                'isPartial',
                'getPartialRequestFields'
            ))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Legacy\DB\DBH\OCI8')
            ->setMethods(array('queryall_array', 'prepare', 'queryfirstcolumn'))
            ->getMock();


        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $this->db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Legacy\DB\STH\OCI8')
            ->disableOriginalConstructor()
            ->setMethods(array('execute', 'fetchrow_assoc'))
            ->getMock();

        $this->db->method('getHandle')->willReturn($this->dbh);
        $this->dbh->method('prepare')->willReturn($this->sth);
        $this->sth->method('execute')->willReturn(true);
        $this->user->method('isAuthorized')->willReturn(true);

        $this->ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        $this->bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();

        $this->app = $this->createMock(App::class);

        $this->student = new \MiamiOH\RestngAcademics\Services\Student();
        $this->student->setApiUser($this->user);
        $this->student->setDatabase($this->db);
        $this->student->setRequest($this->request);
        $this->student->setBannerUtil($this->bannerUtil);
        $this->student->setApp($this->app);

    }

    private function mockOptions($options)
    {
        $this->request
            ->expects($this->once())
            ->method('getOptions')
            ->will($this->returnValue($options));
    }


    private function mockParms($parm)
    {
        $this->request
            ->expects($this->once())
            ->method('getResourceParams')
            ->will($this->returnValue($parm));
    }

    private function mockParmsKey($key)
    {
        $this->request
            ->expects($this->once())
            ->method('getResourceParamKey')
            ->will($this->returnValue($key));
    }


    private function mockPartialRequests($fields)
    {
        $this->request
            ->expects($this->any())
            ->method('isPartial')
            ->will($this->returnValue(true));

        $this->request
            ->expects($this->any())
            ->method('getPartialRequestFields')
            ->will($this->returnValue($fields));
    }

    private function mockQueryAll($order, $val)
    {
        $this->dbh
            ->expects($order)
            ->method('queryall_array')
            ->will($this->returnValue($val));
    }

    private function mockqueryfirstcolumn($order, $val)
    {
        $this->dbh
            ->expects($order)
            ->method('queryfirstcolumn')
            ->will($this->returnValue($val));
    }

    private function assertErrorResponse($errMesg)
    {
        try {
            $resp = $this->student->getStudents();
        } catch (Exception $e) {
            $this->assertEquals($errMesg, $e->getMessage());
        }
    }

    private function assertGoodResponse()
    {
        $resp = $this->student->getStudents();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    }

    private function assertNotFoundResponse()
    {
        $resp = $this->student->getStudents();

        $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $resp->getStatus());
    }

    private function getErrMesg($val)
    {
        if ($val === 'termCode') {
            return 'Invalid termCode option';
        } else {
            if ($val === 'noID') {
                return "There is no IDs to search for.";
            }
        }
    }

    public function testAllValidOptions()
    {
        // pidm query mock
        $this->mockQueryAll($this->at(0), array(array('spriden_pidm' => 237663)));

        // uniqueId query mock
        $this->mockQueryAll($this->at(1), array(array('szbuniq_pidm' => 237663)));

        // registrationFlag query mock
        $this->mockQueryAll($this->at(2), array(array('pidm' => 187663, 'registration_flag' => 'EL')));

        // classCode query mock
        $this->mockQueryAll($this->at(3), array(array('pidm' => 237663, 'class_code' => '10')));

        $this->mockOptions(array(
            'uniqueId' => array('covertka'),
            'pidm' => array(187663),
            'termCode' => 'current',
            'mode' => 'learner',
            'display' => 'normal'
        ));

        $this->mockPartialRequests(array('classCode', 'registrationFlag'));

        $this->assertGoodResponse();
    }

    public function testResponseAllValidOptions()
    {
        // uniqueId query mock
        $this->mockQueryAll($this->at(0), array(array('spriden_pidm' => 237663)));

        // registrationFlag query mock
        $this->mockQueryAll($this->at(1), array(array('pidm' => 187663, 'registration_flag' => 'EL')));

        // classCode query mock
        $this->mockQueryAll($this->at(2), array(array('pidm' => 237663, 'class_code' => '10')));

        $this->mockOptions(array(
            'pidm' => [187663],
            'termCode' => 'current',
            'mode' => 'learner',
            'display' => 'normal'
        ));

        $this->mockPartialRequests(array('classCode', 'registrationFlag'));

        $payload = $this->student->getStudents()->getPayload();

        $this->assertEquals(array(
            array(
                'registrationFlag' => '',
                'classCode' => '10'
            ),
            array(
                'registrationFlag' => 'EL',
            ),
        ),
            $payload);
    }

    public function testPartialClassCodeEmpty()
    {
        $this->mockOptions(array(
            'pidm' => array(187663),
            'termCode' => 'current',
            'mode' => 'learner',
            'display' => 'normal'
        ));

        // pidm query mock
        $this->mockQueryAll($this->at(0), array(array('spriden_pidm' => 187663)));


        // classCode query mock
        $this->mockQueryAll($this->at(1), array());

        $this->mockPartialRequests(array('classCode'));

        $payload = $this->student->getStudents()->getPayload();

        $this->assertEquals(array(
            array('classCode' => '')
        ),
            $payload);
    }

    public function testPartialClassCode()
    {
        $this->mockOptions(array(
            'pidm' => array(187663),
            'termCode' => 'current',
            'mode' => 'learner',
            'display' => 'normal'
        ));

        // pidm query mock
        $this->mockQueryAll($this->at(0), array(array('spriden_pidm' => 187663)));


        // classCode query mock
        $this->mockQueryAll($this->at(1), array(
            array(
                'pidm' => 187663,
                'class_code' => '04'
            )
        ));

        $this->mockPartialRequests(array('classCode'));

        $payload = $this->student->getStudents()->getPayload();

        $this->assertEquals(array(
            array('classCode' => '04')
        ),
            $payload);
    }

    public function testPartialClassCodeMultiple()
    {
        $this->mockOptions(array(
            'pidm' => array(187663, 237663),
            'termCode' => 'current',
            'mode' => 'learner',
            'display' => 'normal'
        ));
        // pidm query mock
        $this->mockQueryAll($this->at(0), array(array('spriden_pidm' => 187663)));


        // classCode query mock
        $this->mockQueryAll($this->at(1), array(
            array(
                'pidm' => 187663,
                'class_code' => '04'
            ),
            array(
                'pidm' => 237663,
                'class_code' => '10'
            )
        ));

        $this->mockPartialRequests(array('classCode'));

        $payload = $this->student->getStudents()->getPayload();

        $this->assertEquals($payload, array(
            array('classCode' => '04'),
            array('classCode' => '10')
        ));
    }
//
//    public function testGetPidm()
//    {
//        $this->mockqueryfirstcolumn($this->at(0), array(array('szbuniq_pidm' => 1445289)));
//
//        $payload = $this->student->getPidm();
//
//        $this->assertEquals($payload,'1445289');
//    }
    public function testTimeTicketNoUniqueIDFound_returnsBadRequest()
    {

        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn('');
        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('uniqueId');

        $this->expectException(\MiamiOH\RESTng\Exception\BadRequest::class);

        $this->response = $this->student->checkTimeTicketByUniqueIdOrPidm();
        $this->assertErrorResponse('Please provide a UniqueId');

    }

    public function testTimeTicketNoPidmFound_returnsBadRequest()
    {

        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn('');
        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('pidm');

        $this->expectException(\MiamiOH\RESTng\Exception\BadRequest::class);

        $this->response = $this->student->checkTimeTicketByUniqueIdOrPidm();
        $this->assertErrorResponse('Please provide a pidm');

    }

    public function testTimeTicketInvalidUniqueID_returnsBadRequest()
    {
        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn('rtr1024*&(');

        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('uniqueId');

        $this->expectException(\MiamiOH\RESTng\Exception\BadRequest::class);

        $this->response = $this->student->checkTimeTicketByUniqueIdOrPidm();

        $this->assertErrorResponse('Unique ID must be alphanumeric, and have less than or equal to 8 letters and digits.');

    }

    public function testTimeTicketInvalidPidm_returnsBadRequest()
    {
        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn('+@1234^');

        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('pidm');

        $this->expectException(\MiamiOH\RESTng\Exception\BadRequest::class);

        $this->response = $this->student->checkTimeTicketByUniqueIdOrPidm();

        $this->assertErrorResponse('pidm must be numeric.');

    }

    public function testForNoValidTimeTicket()
    {

        $this->mockOptions(array('termCode' => '201730'));

        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('uniqueId');

        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn('rtr1021');


        $person = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm', 'getUniqueId', 'getBannerId'))
            ->getMock();

        $person->method('getPidm')
            ->willReturn('610781');

        $this->bannerUtil->method('getId')
            ->with('uniqueId', $this->anything())
            ->willReturn($person);

        $this->dbh->method('queryall_array')->willReturn(array());

        $payload = $this->student->checkTimeTicketByUniqueIdOrPidm()->getPayload();

        $this->assertEquals($this->getNoTimeTicketResponse(), $payload);

    }

    public function testForAllowedToRegisterWithTerm()
    {
        $this->mockOptions(array('termCode' => '201730'));

        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn('rtr1024');

        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('uniqueId');

        $person = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm', 'getUniqueId', 'getBannerId'))
            ->getMock();

        $person->method('getPidm')
            ->willReturn(103409);

        $this->bannerUtil->method('getId')
            ->with('uniqueId', $this->anything())
            ->willReturn($person);


        $this->dbh->method('queryall_array')->willReturn($this->GetTimeTicketQueryResponse());

        $payload = $this->student->checkTimeTicketByUniqueIdOrPidm()->getPayload();

        $this->assertEquals($this->getTimeTicketResponse(), $payload);

    }

    public function testForAllowedToRegisterwithDefaultTerm()
    {
        $responseObject = $this->getMockBuilder('\MiamiOH\RESTng\Util\Response')
            ->setMethods(array('getPayload'))
            ->getMock();

        $responseObject->method('getPayload')->willReturn(array('termId' => '201730'));
        $this->api->method('callResource')->willReturn($responseObject);

        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn('rtr1024');

        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('uniqueId');

        $person = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm', 'getUniqueId', 'getBannerId'))
            ->getMock();

        $person->method('getPidm')
            ->willReturn(103409);

        $this->bannerUtil->method('getId')
            ->with('uniqueId', $this->anything())
            ->willReturn($person);


        $this->dbh->method('queryall_array')->willReturn($this->GetTimeTicketQueryResponse());

        $response = $this->createMock(Response::class);
        $response->method('getPayload')->willReturn(['termId' => 201730]);

        $this->app->method('callResource')->willReturn($response);

        $payload = $this->student->checkTimeTicketByUniqueIdOrPidm()->getPayload();

        $this->assertEquals($this->getTimeTicketResponse(), $payload);

    }

//    public function testForAllowedToRegisterWithValidPidm()
//    {
//        $this->mockOptions(array('termCode' => '201730'));
//
//        $this->request->method('getResourceParam')
//            ->with('muid')
//            ->willReturn(103409);
//
//        $this->request->method('getResourceParamKey')
//            ->with('muid')
//            ->willReturn('pidm');
//
//        $this->dbh->method('queryall_array')->willReturn($this->GetTimeTicketQueryResponse());
//
//        $payload = $this->student->checkTimeTicketByUniqueIdOrPidm()->getPayload();
//
//        $this->assertEquals($this->getTimeTicketResponse(), $payload);
//
//    }

//    public function testForValidTimeticketButNotAllowedToRegisterWithCurrentTerm()
//    {
//        $responseObject = $this->getMockBuilder('\MiamiOH\RESTng\Util\Response')
//            ->setMethods(array('getPayload'))
//            ->getMock();
//
//        $responseObject->method('getPayload')->willReturn(array('termId' => '201730'));
//
//        $this->api->method('callResource')->willReturn($responseObject);
//
//        $this->request->method('getResourceParam')
//            ->with('muid')
//            ->willReturn('rtr1023');
//
//        $this->request->method('getResourceParamKey')
//            ->with('muid')
//            ->willReturn('uniqueId');
//
//        $person = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
//            ->setMethods(array('getPidm', 'getUniqueId', 'getBannerId'))
//            ->getMock();
//
//        $person->method('getPidm')
//            ->willReturn(579898);
//
//        $this->bannerUtil->method('getId')
//            ->with('uniqueId', $this->anything())
//            ->willReturn($person);
//
//
//        $this->dbh->method('queryall_array')->willReturn($this->GetNotValidToRegisterHasTimeTicketQueryResponse());
//
//        $payload = $this->student->checkTimeTicketByUniqueIdOrPidm()->getPayload();
//        $this->assertEquals($this->getNotAbleToRegisterTimeTicketResponse(), $payload);
//
//    }

    public function testSmartDisplay()
    {
        // uniqueId query mock
        $this->mockQueryAll($this->at(0), array(array('szbuniq_pidm' => 187663)));

        // registrationFlag query mock
        $this->mockQueryAll($this->at(1), array(array('pidm' => 187663, 'registration_flag' => 'EL')));

        // classCode query mock
        $this->mockQueryAll($this->at(2), array(array('pidm' => 237663, 'class_code' => '10')));

        $this->mockOptions(array(
            'uniqueId' => array('covertka'),
            'termCode' => 'current',
            'mode' => 'learner',
            'display' => 'smart'
        ));

        $this->mockPartialRequests(array('classCode', 'registrationFlag'));

        $payload = $this->student->getStudents()->getPayload();
        $this->assertEquals($payload, array(
            187663 => array(
                'registrationFlag' => 'EL',
                'classCode' => ''
            ),
            237663 => array(
                'classCode' => '10'
            )
        ));
    }

    /**
     * @return array
     */
    private function getTimeTicketResponse()
    {
        return
            array(
                'pidm' => '103409',
                'termCode' => '201730',
                'priority' => '99',
                'registrationGroup' => 'ND',
                'hasTimeTicket' => 'Y',
                'allowedToRegister' => 'N',
                'registrationTimes' => array(
                    array(
                        'sequenceNo' => '1',
                        'beginDate' => '06-DEC-16',
                        'endDate' => '30-AUG-17',
                        'beginHour' => '0900',
                        'endHour' => '1445'
                    ),
                    array(
                        'sequenceNo' => '2',
                        'beginDate' => '01-SEPT-17',
                        'endDate' => '11-SEPT-17',
                        'beginHour' => '0900',
                        'endHour' => '2359'
                    )
                )
            );
    }

    /**
     * @return array
     */
    private function getNotAbleToRegisterTimeTicketResponse()
    {
        return array(
            'pidm' => '579898',
            'termCode' => '201730',
            'priority' => '99',
            'registrationGroup' => 'ND',
            'hasTimeTicket' => 'Y',
            'allowedToRegister' => 'N',
            'registrationTimes' => array(
                array(
                    'sequenceNo' => '1',
                    'beginDate' => '06-DEC-16',
                    'endDate' => '30-JUN-17',
                    'beginHour' => '0900',
                    'endHour' => '1445'
                ),
                array(
                    'sequenceNo' => '2',
                    'beginDate' => '01-JUL-17',
                    'endDate' => '11-JUL-17',
                    'beginHour' => '0900',
                    'endHour' => '2359'
                )
            )
        );
    }

    /**
     * @return array
     */
    private function GetTimeTicketQueryResponse()
    {
        return array(
            array(
                'termcode' => '201730',
                'priority' => '99',
                'sequenceno' => '1',
                'begindate' => '06-DEC-16',
                'enddate' => '30-AUG-17',
                'beginhour' => '0900',
                'endhour' => '1445',
                'registrationgroup' => 'ND'
            ),
            array(
                'termcode' => '201730',
                'priority' => '99',
                'sequenceno' => '2',
                'begindate' => '01-SEPT-17',
                'enddate' => '11-SEPT-17',
                'beginhour' => '0900',
                'endhour' => '2359',
                'registrationgroup' => 'ND'
            )
        );
    }

    /**
     * @return array
     */
    private function GetNotValidToRegisterHasTimeTicketQueryResponse()
    {
        return array(
            array(
                'termcode' => '201730',
                'priority' => '99',
                'sequenceno' => '1',
                'begindate' => '06-DEC-16',
                'enddate' => '30-JUN-17',
                'beginhour' => '0900',
                'endhour' => '1445',
                'registrationgroup' => 'ND'
            ),
            array(
                'termcode' => '201730',
                'priority' => '99',
                'sequenceno' => '2',
                'begindate' => '01-JUL-17',
                'enddate' => '11-JUL-17',
                'beginhour' => '0900',
                'endhour' => '2359',
                'registrationgroup' => 'ND'
            )
        );
    }

    /**
     * @return array
     */
    private function getNoTimeTicketResponse()
    {
        return array(
            'pidm' => '610781',
            'termCode' => '201730',
            'priority' => '',
            'registrationGroup' => '',
            'hasTimeTicket' => 'N',
            'allowedToRegister' => 'N',
            'registrationTimes' => array()
        );
    }


    // public function testNotFound(){
    //     // uniqueId query mock
    //     $this->mockQueryAll($this->at(0), array(array('szbuniq_pidm' => 237663)));

    //     // registrationFlag query mock
    //     $this->mockQueryAll($this->at(1), array());

    //     // classCode query mock
    //     $this->mockQueryAll($this->at(2), array());

    //     $this->mockOptions(array('uniqueId' => array('covertka'),
    //                              'pidm' => array(187663),
    //                              'termCode' => 'current',
    //                              'mode' => 'learner',
    //                              'display' => 'normal'));

    //     $this->mockPartialRequests(array('classCode', 'registrationFlag'));

    //     $resp = $this->student->getStudents();

    //     $this->assertNotFoundResponse();
    // }

    // // Controversial - current argument - a record with no property has no meaning and value.
    // // If record does not exist, consumer can safely assume data is either null or empty.
    // public function testNotReturnIfNothingInProperty(){
    //     // uniqueId query mock
    //     $this->mockQueryAll($this->at(0), array(array('szbuniq_pidm' => 237663)));

    //     // registrationFlag query mock
    //     $this->mockQueryAll($this->at(1), array(array('pidm' => 187663, 'registration_flag' => 'EL')));

    //     // classCode query mock
    //     $this->mockQueryAll($this->at(2), array());

    //     $this->mockOptions(array('uniqueId' => array('covertka'),
    //                              'pidm' => array(187663),
    //                              'termCode' => 'current',
    //                              'mode' => 'learner',
    //                              'display' => 'normal'));

    //     $this->mockPartialRequests(array('classCode', 'registrationFlag'));

    //     $payload = $this->student->getStudents()->getPayload();

    //     $this->assertEquals(array(
    //         array(
    //             'pidm' => 187663,
    //             'registrationFlag' => 'EL',
    //             'classCode' => '')),
    //                         $payload);
    // }
}
