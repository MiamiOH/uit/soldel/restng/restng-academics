<?php

namespace MiamiOH\RestngAcademics\Tests\Feature;

use MiamiOH\RestngAcademics\Data\DataLoaderLocal;
use MiamiOH\RestngAcademics\Tests\FeatureTestCase;

class StudentGpaTest extends FeatureTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->useService([
            'name' => 'Student.DataLoader',
            'object' => new DataLoaderLocal(),
            'description' => 'Student data loader',
        ]);
    }

    public function testReturnsNotFoundIfUniqueIdIsNotFoundForTermRecords(): void
    {
        $response = $this->getJson('/student/academics/v1/publicjq/gpa');

        $response->assertStatus(404);
    }

    public function testCanGetStudentGpaTermRecords(): void
    {
        $response = $this->getJson('/student/academics/v1/doej/gpa');

        $response->assertStatus(200);

        $body = json_decode($response->content(), true);
        $this->assertTrue(array_key_exists('data', $body));

        $data = $body['data'];
        $this->assertCount(1, $data);

        $gpaRecord = $data[0];
        $this->assertEquals(
            [
                'uniqueId' => 'doej',
                'termCode' => '202210',
                'studentLevel' => 'UG',
                'gpaType' => 'I',
                'hoursEarned' => 16,
                'gpaHours' => 16,
                'gpaQualityPoints' => 41,
                'gpa' => 2.56,
            ],
            $gpaRecord
        );
    }

    public function testReturnsNotFoundIfUniqueIdIsNotFoundForCumulativeRecords(): void
    {
        $response = $this->getJson('/student/academics/v1/publicjq/gpa/cumulative');

        $response->assertStatus(404);
    }

    public function testCanGetStudentGpaCumulativeRecords(): void
    {
        $response = $this->getJson('/student/academics/v1/doej/gpa/cumulative');

        $response->assertStatus(200);

        $body = json_decode($response->content(), true);
        $this->assertTrue(array_key_exists('data', $body));

        $data = $body['data'];
        $this->assertCount(1, $data);

        $gpaRecord = $data[0];
        $this->assertEquals(
            [
                'uniqueId' => 'doej',
                'studentLevel' => 'UG',
                'gpaType' => 'I',
                'hoursEarned' => 16,
                'gpaHours' => 16,
                'gpaQualityPoints' => 41,
                'gpa' => 2.56,
            ],
            $gpaRecord
        );
    }
}
