<?php

namespace MiamiOH\RestngAcademics\Tests;

use MiamiOH\RESTng\Testing\TestCase;
use MiamiOH\RESTng\Testing\UsesAuthentication;
use MiamiOH\RESTng\Testing\UsesAuthorization;

abstract class FeatureTestCase extends TestCase
{
    use UsesAuthentication;
    use UsesAuthorization;

    public function setUp(): void
    {
        parent::setUp();

        $this->willAuthenticateUser();

        $this->willAuthorizeUser();

    }
}
